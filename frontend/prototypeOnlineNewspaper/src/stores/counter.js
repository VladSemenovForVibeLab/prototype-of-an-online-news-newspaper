import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
export const useCounterStore = defineStore('counter', () => {
  const count = ref(0)
  const doubleCount = computed(() => count.value * 2)
  function increment() {
    count.value++
  }
  return { count, doubleCount, increment }
})
export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    accessToken: null,
    currentUser: {
      username: '',
      firstname: '',
      lastName: '',
      password: '',
      passwordConfirmation: ''
    }, // Добавляем состояние для информации о текущем пользователе
  }),
  actions: {
    setAccessToken(token) {
      this.accessToken = token;
    },
    getAccessToken() {
      return this.accessToken;
    },
    setCurrentUser(user) {
      // Обновляем информацию о текущем пользователе
      this.currentUser = {
        ...this.currentUser,
        ...user
      };
    },
    getCurrentUser() {
      return this.currentUser; // Метод для получения информации о текущем пользователе
    },
    saveLoginResponse(response) {
      // Сохраняем JSON-ответ в состояние
      this.currentUser = response;

      // Сохраняем accessToken в локальном хранилище
      localStorage.setItem('accessToken', response.accessToken);
    },
  },
});
export const useAppStore = defineStore('app', {
  state: () => ({
    loginForm: {
      username: '',
      password: ''
    },
    loginMessage: '',
    userData: {
      id: 1,
      username: 'user1@example.com',
      accessToken: '',
      refreshToken: ''
    }
  }),
  actions: {
    setLoginForm(username, password) {
      this.loginForm.username = username;
      this.loginForm.password = password;
    },
    setLoginMessage(message) {
      this.loginMessage = message;
    },
    setUserData(userData) {
      this.userData = userData;
    }
  }
});
