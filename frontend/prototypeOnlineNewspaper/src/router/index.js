import { createRouter, createWebHistory } from 'vue-router'
import AuthView from "@/components/AuthView.vue";
import RegistrationView from "@/components/RegistrationView.vue";
import HomeViewMy from "@/components/HomeViewMy.vue";
import UserProfile from "@/components/UserProfile.vue";
import AdminPanel from "@/components/AdminPanel.vue";
import AdminPanelHomeView from "@/components/AdminPanelHomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'Login',
      component: AuthView
    },

    {
      path: '/admin-home',
      name: 'Admin',
      component: AdminPanelHomeView
    },
    {
      path: '/user-profile',
      name: 'User profile',
      component: UserProfile
    },
    {
      path: '/',
      name: 'Register',
      component: RegistrationView
    },
    {
      path: '/home',
      name: 'Home',
      component: HomeViewMy
    },
    {
      path: '/admin',
      name: 'Admin',
      component: AdminPanel,
      meta: { requiresAdmin: true }, // Добавьте мета-информацию, если нужно проверять роль администратора
    },
  ]
})

export default router
