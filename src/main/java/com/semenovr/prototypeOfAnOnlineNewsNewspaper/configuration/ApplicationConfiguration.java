package com.semenovr.prototypeOfAnOnlineNewsNewspaper.configuration;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.JwtTokenFilter;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;

/**
 * Конфигурация приложения для обработки аутентификации и авторизации.
 */
@Configuration
@EnableWebSecurity
@EnableMethodSecurity
@RequiredArgsConstructor(onConstructor = @__(@Lazy))
public class ApplicationConfiguration {
    private final JwtTokenProvider tokenProvider;

    /**
     * Создает бин для кодирования паролей с использованием BCrypt.
     *
     * @return Объект кодировщика паролей.
     */
    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    /**
     * Создает бин для управления аутентификацией.
     *
     * @param configuration Объект конфигурации аутентификации.
     * @return Менеджер аутентификации.
     * @throws Exception В случае ошибок конфигурации аутентификации.
     */
    @Bean
    public AuthenticationManager authenticationManager(AuthenticationConfiguration configuration) throws Exception {
        return configuration.getAuthenticationManager();
    }

    /**
     * Создает цепочку фильтров безопасности HTTP.
     *
     * @param httpSecurity Объект конфигурации HTTP безопасности.
     * @return Цепочка фильтров безопасности HTTP.
     * @throws Exception В случае ошибок конфигурации HTTP безопасности.
     */
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf(AbstractHttpConfigurer::disable)
                .cors(cors -> {
                    cors.configurationSource(request -> {
                        CorsConfiguration corsConfig = new CorsConfiguration();
                        corsConfig.addAllowedOrigin("http://localhost:5173");
                        corsConfig.addAllowedMethod("*");
                        corsConfig.addAllowedHeader("*");
                        return corsConfig;
                    });
                })
                .httpBasic(AbstractHttpConfigurer::disable)
                .sessionManagement(sess -> sess.sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .exceptionHandling(exceptionHandling -> exceptionHandling
                        .authenticationEntryPoint((request, response, authException) -> {
                            response.setStatus(HttpStatus.UNAUTHORIZED.value());
                            response.getWriter().write("Unauthorized");
                        })
                        .accessDeniedHandler(((request, response, accessDeniedException) -> {
                            response.setStatus(HttpStatus.FORBIDDEN.value());
                            response.getWriter().write("Unauthorized");
                        })))
                .authorizeRequests(authorizeRequests -> authorizeRequests
                        .antMatchers("/api/v1/auth/**").permitAll()
                        .antMatchers("/api/v1/themes/{id}").permitAll()
                        .antMatchers("/api/v1/themes").permitAll()
                        .antMatchers("/api/v1/comments/article").permitAll()
                        .antMatchers("/api/v1/comments").permitAll()
                        .antMatchers("/api/v1/articles/latest-articles").permitAll()
                        .antMatchers("/api/v1/articles/all").permitAll()
                        .antMatchers("/api/v1/articles/find/{id}").permitAll()
                        .anyRequest().authenticated()
                )
                .addFilterBefore(new JwtTokenFilter(tokenProvider), UsernamePasswordAuthenticationFilter.class);
        return httpSecurity.build();
    }
}
