package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.ArticleEditHistory;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.springframework.data.convert.ReadingConverter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий для работы с историей редактирования статей.
 * Этот репозиторий предоставляет методы для поиска и работы с записями истории редактирования статей.
 */
@Repository
public interface ArticleEditHistoryRepository extends JpaRepository<ArticleEditHistory, Long> {

    /**
     * Найти все записи истории редактирования для данной статьи и отсортировать их по дате редактирования в убывающем порядке.
     *
     * @param article Статья, для которой необходимо найти историю редактирования.
     * @return Список записей истории редактирования, отсортированных по дате редактирования в убывающем порядке.
     */
    List<ArticleEditHistory> findByArticleOrderByEditDateDesc(Article article);


    /**
     * Найти все записи истории редактирования, сделанные администратором.
     *
     * @param user Администратор, чьи записи истории редактирования нужно найти.
     * @return Список записей истории редактирования, сделанных указанным администратором.
     */
    List<ArticleEditHistory> findByAdministrator(User user);

    /**
     * Проверить наличие записи истории редактирования для данной статьи, выполненной указанным администратором.
     *
     * @param article     Статья, для которой нужно проверить наличие записи истории редактирования.
     * @param administrator Администратор, чья запись истории редактирования нужно найти.
     * @return true, если запись истории редактирования существует, иначе false.
     */
    boolean existsByArticleAndAdministrator(Article article, User administrator);
}