package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.UserSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий для управления данными настроек пользователя.
 */
@Repository
public interface UserSettingsRepository extends JpaRepository<UserSettings, Long> {

    /**
     * Найти список настроек пользователя по его избранным темам.
     *
     * @param object Список избранных тем для поиска настроек пользователя.
     * @return Список настроек пользователя, связанных с указанными избранными темами.
     */
    List<UserSettings> findUserSettingsByFavoriteThemes(Theme object);

    /**
     * Найти список настроек пользователя по его запрещенным темам.
     *
     * @param object Список запрещенных тем для поиска настроек пользователя.
     * @return Список настроек пользователя, связанных с указанными запрещенными темами.
     */
    List<UserSettings> findUserSettingByForbiddenThemes(Theme object);
}