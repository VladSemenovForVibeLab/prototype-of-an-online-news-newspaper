package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Репозиторий для работы с комментариями к статьям.
 * Этот репозиторий предоставляет методы для поиска и работы с комментариями к статьям.
 */
@Repository
public interface CommentRepository extends JpaRepository<Comment, Long> {

    /**
     * Найти и вернуть список комментариев, связанных с указанной статьей, отсортированных по дате написания в убывающем порядке.
     *
     * @param article Статья, для которой нужно найти комментарии.
     * @return Список комментариев, связанных с указанной статьей, отсортированных по дате написания в убывающем порядке.
     */
    List<Comment> findByArticleOrderByWrittenDateDesc(Article article);
}