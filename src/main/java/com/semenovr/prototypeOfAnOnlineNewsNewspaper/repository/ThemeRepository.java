package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Репозиторий для работы с темами новостей.
 * Этот репозиторий предоставляет методы для поиска и работы с темами новостей.
 */
@Repository
public interface ThemeRepository extends JpaRepository<Theme, Long> {

    /**
     * Проверить наличие темы новостей с указанным именем.
     *
     * @param name Имя темы для проверки наличия.
     * @return true, если тема с указанным именем существует, иначе false.
     */
    boolean existsByName(String name);
}