package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Репозиторий для управления данными о пользователях.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    /**
     * Поиск пользователя по его имени пользователя.
     *
     * @param username Имя пользователя для поиска.
     * @return Пользователь с указанным именем пользователя или пустой результат, если пользователь не найден.
     */
    Optional<User> findByUsername(String username);
}