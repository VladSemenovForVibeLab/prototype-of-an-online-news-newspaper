package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Репозиторий для работы с лайками к статьям.
 * Этот репозиторий предоставляет методы для поиска и работы с лайками, связанными со статьями.
 */
@Repository
public interface LikeRepository extends JpaRepository<Like, Long> {

    /**
     * Найти лайк, сделанный указанным пользователем для указанной статьи, если такой лайк существует.
     *
     * @param user   Пользователь, сделавший лайк.
     * @param article Статья, к которой привязан лайк.
     * @return Optional с найденным лайком или пустой, если лайк не найден.
     */
    Optional<Like> findByUserAndArticle(User user, Article article);

    /**
     * Найти лайк, сделанный указанным пользователем для указанной статьи.
     *
     * @param article Статья, к которой привязан лайк.
     * @param user    Пользователь, сделавший лайк.
     * @return Найденный лайк или null, если лайк не найден.
     */
    Like findByArticleAndUser(Article article, User user);

    /**
     * Проверить наличие лайка, сделанного указанным пользователем для указанной статьи.
     *
     * @param article Статья, к которой привязан лайк.
     * @param user    Пользователь, сделавший лайк.
     * @return true, если лайк существует, иначе false.
     */
    boolean existsByArticleAndUser(Article article, User user);

    /**
     * Найти и вернуть список лайков, связанных с указанной статьей.
     *
     * @param object Статья, для которой нужно найти лайки.
     * @return Список лайков, связанных с указанной статьей.
     */
    List<Like> findByArticle(Article object);
}