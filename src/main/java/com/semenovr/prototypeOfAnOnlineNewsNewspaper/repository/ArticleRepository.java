package com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Репозиторий для работы со статьями.
 * Этот репозиторий предоставляет методы для поиска и работы со статьями.
 */
@Repository
public interface ArticleRepository extends JpaRepository<Article, Long> {

    /**
     * Найти и вернуть список статей, опубликованных после указанной даты, отсортированных по дате публикации в убывающем порядке.
     *
     * @param date Дата, после которой нужно искать статьи.
     * @return Список статей, опубликованных после указанной даты, отсортированных по дате публикации в убывающем порядке.
     */
    List<Article> findByPublicationDateAfterOrderByPublicationDateDesc(LocalDateTime date);


    /**
     * Проверить наличие статьи с указанным заголовком.
     *
     * @param articleTitle Заголовок статьи для проверки наличия.
     * @return true, если статья с указанным заголовком существует, иначе false.
     */
    boolean existsByTitle(String articleTitle);
}