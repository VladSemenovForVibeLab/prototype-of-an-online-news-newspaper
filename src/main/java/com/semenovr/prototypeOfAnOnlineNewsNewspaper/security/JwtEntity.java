package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * Класс, представляющий объект аутентификации для использования в системе безопасности.
 */
@Data
@AllArgsConstructor
public class JwtEntity implements UserDetails {
    private Long id;
    private final String username;
    private final String firstname;
    private final String lastName;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorities;

    /**
     * Возвращает коллекцию ролей, связанных с пользователем.
     *
     * @return Коллекция ролей пользователя.
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }


    /**
     * Возвращает пароль пользователя.
     *
     * @return Пароль пользователя.
     */
    @Override
    public String getPassword() {
        return password;
    }

    /**
     * Возвращает имя пользователя.
     *
     * @return Имя пользователя.
     */
    @Override
    public String getUsername() {
        return username;
    }


    /**
     * Проверяет, истек ли срок действия учетной записи пользователя.
     *
     * @return Всегда возвращает true, так как срок действия учетной записи не истекает.
     */
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**
     * Проверяет, заблокирована ли учетная запись пользователя.
     *
     * @return Всегда возвращает true, так как учетная запись не блокируется.
     */
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**
     * Проверяет, истек ли срок действия учетных данных пользователя.
     *
     * @return Всегда возвращает true, так как учетные данные пользователя не истекают.
     */
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    /**
     * Проверяет, включен ли пользователь.
     *
     * @return Всегда возвращает true, так как пользователь всегда включен.
     */
    @Override
    public boolean isEnabled() {
        return true;
    }
}


