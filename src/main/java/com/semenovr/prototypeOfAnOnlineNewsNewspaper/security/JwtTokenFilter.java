package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ResourceNotFoundException;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Фильтр для обработки JWT-токена в запросах.
 */
@AllArgsConstructor
public class JwtTokenFilter extends GenericFilterBean {
    private final JwtTokenProvider jwtTokenProvider;

    /**
     * Выполняет фильтрацию запроса для обработки JWT-токена.
     *
     * @param servletRequest  Запрос, поступивший от клиента.
     * @param servletResponse Ответ, который будет отправлен клиенту.
     * @param filterChain     Цепочка фильтров для обработки запроса.
     * @throws IOException      Возникает при ошибках ввода/вывода.
     * @throws ServletException Возникает при ошибках сервлета.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String bearerToken = ((HttpServletRequest)servletRequest).getHeader("Authorization");
        if(bearerToken!=null&&bearerToken.startsWith("Bearer ")){
            bearerToken=bearerToken.substring(7);
        }
        if(bearerToken!=null&& jwtTokenProvider.validateToken(bearerToken)){
            try {
                Authentication authentication = jwtTokenProvider.getAuthentication(bearerToken);
                if(authentication!=null){
                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }catch (ResourceNotFoundException ignored){
                // Игнорируем исключение ResourceNotFoundException
            }
        }
        filterChain.doFilter(servletRequest,servletResponse);
    }
}

