package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.JwtProperties;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.AccessDeniedException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Класс, предоставляющий функциональность для создания и валидации JWT-токенов, а также обновления токенов.
 */
@Service
@RequiredArgsConstructor
public class JwtTokenProvider {
    private final JwtProperties jwtProperties;
    private final UserDetailsService userDetailsService;
    private final UserService userService;

    /**
     * Инициализирует ключ для подписи JWT-токенов при создании экземпляра класса.
     */
    private Key key;

    @PostConstruct
    public void init(){
        this.key= Keys.hmacShaKeyFor(jwtProperties.getSecret().getBytes());
    }


    /**
     * Создает доступный JWT-токен на основе идентификатора пользователя, его имени пользователя и ролей.
     *
     * @param userId   Идентификатор пользователя.
     * @param username Имя пользователя.
     * @param roles    Роли пользователя.
     * @return Строка с JWT-токеном доступа.
     */
    public String createAccessToken(Long userId, String username, Set<Role> roles){
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("id",userId);
        claims.put("roles",resolveRoles(roles));
        Instant validity = Instant.now()
                .plus(jwtProperties.getAccess(), ChronoUnit.HOURS);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(validity))
                .signWith(key)
                .compact();
    }


    /**
     * Преобразует роли пользователя в список строк.
     *
     * @param roles Роли пользователя.
     * @return Список строк, представляющих роли пользователя.
     */
    private List<String> resolveRoles(Set<Role> roles) {
        return roles.stream()
                .map(Enum::name)
                .collect(Collectors.toList());
    }

    /**
     * Создает токен обновления на основе идентификатора пользователя и имени пользователя.
     *
     * @param userId   Идентификатор пользователя.
     * @param username Имя пользователя.
     * @return Строка с токеном обновления.
     */
    public String createRefreshToken(Long userId,String username){
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("id",userId);
        Instant validity = Instant.now()
                .plus(jwtProperties.getRefresh(), ChronoUnit.HOURS);
        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(Date.from(validity))
                .signWith(key)
                .compact();
    }

    /**
     * Обновляет JWT-токены пользователя на основе токена обновления.
     *
     * @param refreshToken Токен обновления.
     * @return Объект JWTResponse с новыми токенами пользователя.
     */
    public JWTResponse refreshUserTokens(String refreshToken){
        JWTResponse jwtResponse= new JWTResponse();
        if(!validateToken(refreshToken)){
            throw new AccessDeniedException();
        }
        Long userId = Long.valueOf(getId(refreshToken));
        User user = userService.findById(userId);
        jwtResponse.setId(userId);
        jwtResponse.setUsername(user.getUsername());
        jwtResponse.setAccessToken(createAccessToken(userId,user.getUsername(),user.getRoles()));
        jwtResponse.setRefreshToken(createRefreshToken(userId,user.getUsername()));
        return jwtResponse;
    }

    /**
     * Извлекает идентификатор пользователя из JWT-токена.
     *
     * @param token JWT-токен.
     * @return Строка с идентификатором пользователя.
     */
    private String getId(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .get("id")
                .toString();
    }

    /**
     * Проверяет валидность JWT-токена.
     *
     * @param token JWT-токен.
     * @return true, если токен действителен; в противном случае - false.
     */
    public boolean validateToken(String token) {
        Jws<Claims> claims = Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token);
        return !claims.getBody().getExpiration().before(new Date());
    }

    /**
     * Метод получает объект аутентификации на основе переданного токена.
     *
     * @param token Токен, на основе которого нужно создать аутентификацию.
     * @return Объект аутентификации для пользователя, связанного с переданным токеном.
     */
    public Authentication getAuthentication(String token){
        // Извлекаем имя пользователя из токена
        String username = getUsername(token);
        // Загружаем сведения о пользователе из сервиса пользователей
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        // Создаем и возвращаем объект аутентификации
        return new UsernamePasswordAuthenticationToken(userDetails,"",userDetails.getAuthorities());
    }

    /**
     * Метод извлекает имя пользователя из JWT-токена.
     *
     * @param token JWT-токен, из которого нужно извлечь имя пользователя.
     * @return Имя пользователя, извлеченное из JWT-токена.
     */
    private String getUsername(String token) {
        return Jwts
                .parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();
    }
}

