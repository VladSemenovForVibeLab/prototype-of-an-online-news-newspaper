package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.expression;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.JwtEntity;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Сервис, предоставляющий пользовательские выражения для оценки безопасности.
 */
@Service("customSecurityExpression")
@RequiredArgsConstructor
public class CustomSecurityExpression {
    private final UserService userService;

    /**
     * Проверяет, имеет ли текущий пользователь доступ к определенному пользователю по его идентификатору.
     *
     * @param id Идентификатор пользователя, к которому проверяется доступ.
     * @return true, если текущий пользователь имеет доступ к указанному пользователю; в противном случае - false.
     */
    public boolean canAccessUser(Long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        JwtEntity user = (JwtEntity) authentication.getPrincipal();
        Long userId = user.getId();
        return userId.equals(id) || hasAnyRole(authentication, Role.ROLE_ADMIN);
    }
    private boolean hasAnyRole(Authentication authentication, Role... roles) {
        for(Role role:roles){
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.name());
            if(authentication.getAuthorities().contains(authority)){
                return true;
            }
        }
        return false;
    }
}
