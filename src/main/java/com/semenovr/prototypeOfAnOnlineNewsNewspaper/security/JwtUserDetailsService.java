package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;


import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


/**
 * Класс JwtUserDetailsService представляет собой сервис Spring Security,
 * который используется для загрузки информации о пользователе по его имени пользователя.
 * Он реализует интерфейс UserDetailsService и является частью аутентификационного
 * и авторизационного механизма приложения.
 */
@Service
@RequiredArgsConstructor
public class JwtUserDetailsService implements UserDetailsService {

    private final UserService userService;


    /**
     * Метод loadUserByUsername загружает информацию о пользователе по его имени пользователя.
     * Этот метод вызывается Spring Security при попытке аутентификации пользователя.
     *
     * @param username Имя пользователя, для которого нужно загрузить информацию.
     * @return Объект UserDetails, представляющий информацию о пользователе.
     * @throws UsernameNotFoundException если пользователь с указанным именем не найден.
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.getByUsername(username);
        return JwtEntityFactory.create(user);
    }
}

