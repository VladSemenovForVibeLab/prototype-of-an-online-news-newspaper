package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.JwtEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Фабрика для создания объектов JwtEntity на основе данных пользователя.
 */
public class JwtEntityFactory {

    /**
     * Создает объект JwtEntity на основе объекта пользователя.
     *
     * @param user Объект пользователя для создания JwtEntity.
     * @return Созданный объект JwtEntity.
     */
    public static JwtEntity create(User user) {
        return new JwtEntity(
                user.getId(),
                user.getUsername(),
                user.getFirstname(),
                user.getLastName(),
                user.getPassword(),
                mapToGrantedAuthorities(new ArrayList<>(user.getRoles()))
        );
    }

    /**
     * Преобразует список ролей в список объектов GrantedAuthority.
     *
     * @param roles Список ролей для преобразования.
     * @return Список объектов GrantedAuthority, представляющих роли пользователя.
     */
    private static List<GrantedAuthority> mapToGrantedAuthorities(List<Role> roles) {
        return roles.stream()
                .map(Enum::name)
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());
    }
}

