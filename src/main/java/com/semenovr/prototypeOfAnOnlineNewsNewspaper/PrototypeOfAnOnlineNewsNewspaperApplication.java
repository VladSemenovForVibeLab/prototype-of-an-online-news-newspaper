package com.semenovr.prototypeOfAnOnlineNewsNewspaper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Класс PrototypeOfAnOnlineNewsNewspaperApplication представляет собой точку входа в приложение.
 * Он использует аннотацию @SpringBootApplication для настройки и автоматической конфигурации
 * приложения Spring Boot.
 */
@SpringBootApplication
public class PrototypeOfAnOnlineNewsNewspaperApplication {

	/**
	 * Метод main является точкой входа в приложение.
	 *
	 * @param args Аргументы командной строки, передаваемые при запуске приложения.
	 */
	public static void main(String[] args) {
		SpringApplication.run(PrototypeOfAnOnlineNewsNewspaperApplication.class, args);
	}

}
