package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Класс Article представляет сущность статьи в системе онлайн-газеты.
 * Этот класс аннотирован как сущность JPA и представляет записи в таблице "articles" базы данных.
 */
@Getter
@Setter
@Entity
@Table(name = "articles")
public class Article {
    // Уникальный идентификатор статьи
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    // Заголовок статьи
    private String title;
    // Текст статьи
    @Column(length = 1000)
    private String text;

    // URL изображения, связанного со статьей
    private String imageUrl;

    // Дата публикации статьи
    private LocalDateTime publicationDate;

    // Автор статьи, связанный с сущностью User
    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    // Список комментариев, связанных с данной статьей
    @OneToMany(mappedBy = "article")
    private List<Comment> comments;

    // Список тем, к которым относится данная статья, через связь Many-to-Many
    @ManyToMany
    @JoinTable(name = "article_theme",
            joinColumns = @JoinColumn(name = "article_id"),
            inverseJoinColumns = @JoinColumn(name = "theme_id"))
    private Set<Theme> themes;

    // Метод, выполняемый перед сохранением новой статьи
    @PrePersist
    public void init(){
        this.publicationDate=LocalDateTime.now();
    }

    // Метод, выполняемый перед обновлением существующей статьи
    @PreUpdate
    public void update(){
        this.publicationDate=LocalDateTime.now();
    }
}