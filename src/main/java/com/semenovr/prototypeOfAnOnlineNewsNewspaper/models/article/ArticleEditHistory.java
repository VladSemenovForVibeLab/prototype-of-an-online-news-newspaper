package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Класс ArticleEditHistory представляет сущность истории редактирования статьи в системе онлайн-газеты.
 * Этот класс аннотирован как сущность JPA и представляет записи в таблице "article_edit_history" базы данных.
 */
@Getter
@Setter
@Entity
@Table(name = "article_edit_history")
public class ArticleEditHistory {
    // Уникальный идентификатор записи истории редактирования статьи
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    // Статья, к которой относится данная запись истории редактирования
    @ManyToOne
    @JoinColumn(name = "article_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Article article;

    // Дата редактирования записи
    private LocalDateTime editDate;

    // Администратор, выполнивший редактирование статьи (может быть null)
    @ManyToOne
    @JoinColumn(name = "administrator_id",nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User administrator;

    // Метод, выполняемый перед сохранением новой записи истории редактирования
    @PrePersist
    public void init(){
        this.editDate=LocalDateTime.now();
    }

    // Метод, выполняемый перед обновлением существующей записи истории редактирования
    @PreUpdate
    public void update(){
        this.editDate=LocalDateTime.now();
    }
}