package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Класс, представляющий сущность "Пользователь" (User).
 * Этот класс аннотирован как сущность JPA, что позволяет сохранять его в базе данных.
 */
@Data
@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"articles", "comments", "likes", "userSettings"})
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "username")
    private String username;
    @Column(name = "firstname")
    private String firstname;
    private String lastName;
    private String password;
    @Transient
    private String passwordConfirmation;
    @Column(name = "role")
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "users_roles")
    @Enumerated(value = EnumType.STRING)
    private Set<Role> roles;
    @OneToMany(mappedBy = "author",cascade = CascadeType.DETACH)
    private List<Article> articles;

    @OneToMany(mappedBy = "author")
    private List<Comment> comments;

    @OneToMany(mappedBy = "user",orphanRemoval = true)
    private Set<Like> likes;

    @OneToOne(mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval = true)
    private UserSettings userSettings;
}
