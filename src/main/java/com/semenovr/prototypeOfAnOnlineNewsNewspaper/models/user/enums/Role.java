package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums;

/**
 * Перечисление, представляющее роли пользователей.
 * - ROLE_USER: Роль обычного пользователя.
 * - ROLE_ADMIN: Роль администратора.
 * Это перечисление определяет доступные роли пользователей в системе.
 */
public enum Role {
    ROLE_USER, // Роль обычного пользователя.
    ROLE_ADMIN // Роль администратора.
}

