package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс UserSettings представляет настройки пользователя в системе.
 * Этот класс содержит информацию о пользовательских настройках, таких как избранные и запрещенные темы.
 * Он связан с сущностью User и с темами (Theme) через отношения Many-to-Many.
 */
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserSettings {

    /**
     * Уникальный идентификатор настроек пользователя.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Связь с сущностью User, к которой привязаны эти настройки.
     */
    @OneToOne
    @JoinColumn(name = "user_id")
    @JsonBackReference
    private User user;

    /**
     * Список избранных тем пользователя.
     * Связь Many-to-Many с сущностью Theme через таблицу user_favorite_themes.
     */
    @ManyToMany
    @JoinTable(name = "user_favorite_themes",
            joinColumns = @JoinColumn(name = "user_settings_id"),
            inverseJoinColumns = @JoinColumn(name = "theme_id"))
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    private Set<Theme> favoriteThemes;

    /**
     * Список запрещенных тем пользователя.
     * Связь Many-to-Many с сущностью Theme через таблицу user_forbidden_themes.
     */
    @ManyToMany
    @JoinTable(name = "user_forbidden_themes",
            joinColumns = @JoinColumn(name = "user_settings_id"),
            inverseJoinColumns = @JoinColumn(name = "theme_id"))
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    private Set<Theme> forbiddenThemes;
}