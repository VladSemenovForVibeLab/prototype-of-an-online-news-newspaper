package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое при обнаружении дублирующей комбинации элементов.
 */
public class DuplicateCombinationException extends RuntimeException {

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param s Сообщение об ошибке, описывающее причину исключения.
     */
    public DuplicateCombinationException(String s) {
        super(s);
    }
}
