package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое при попытке доступа к ресурсу, который не существует.
 */
public class ResourceNotFoundException  extends RuntimeException{

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения при попытке доступа к ресурсу, который не существует.
     */
    public ResourceNotFoundException(String message) {
        super(message);
    }
}

