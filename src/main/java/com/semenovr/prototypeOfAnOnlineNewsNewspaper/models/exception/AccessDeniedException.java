package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Класс AccessDeniedException представляет исключение, которое может возникнуть в приложении,
 * когда у пользователя отсутствует доступ к определенному ресурсу или действию.
 * Это исключение является подклассом RuntimeException.
 */
public class AccessDeniedException extends RuntimeException{
    /**
     * Конструктор по умолчанию для исключения AccessDeniedException.
     */
    public AccessDeniedException(){
        super();
    }
}

