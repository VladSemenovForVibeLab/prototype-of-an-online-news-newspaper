package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;


/**
 * Исключение, выбрасываемое при попытке доступа к "лайку", который не существует.
 */
public class LikeNotFoundException extends RuntimeException {

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения при попытке доступа к "лайку", который не существует.
     */
    public LikeNotFoundException(String message) {
        super(message);
    }
}
