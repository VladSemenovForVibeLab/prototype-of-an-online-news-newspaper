package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;


/**
 * Исключение, выбрасываемое в случае, если имя темы уже существует.
 */
public class ThemeNameAlreadyExistsException extends RuntimeException{

    /**
     * Создает новое исключение с указанным сообщением.
     *
     * @param message Сообщение об ошибке.
     */
    public ThemeNameAlreadyExistsException(String message) {
        super(message);
    }
}
