package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое в случае, если пользователь не найден.
 */
public class UserNotFoundException extends RuntimeException{

    /**
     * Создает новое исключение с указанным сообщением.
     *
     * @param message Сообщение об ошибке.
     */
    public UserNotFoundException(String message){
        super(message);
    }
}
