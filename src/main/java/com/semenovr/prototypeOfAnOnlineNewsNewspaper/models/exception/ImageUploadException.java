package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;


/**
 * Исключение, выбрасываемое при возникновении ошибок при загрузке изображения.
 */
public class ImageUploadException extends RuntimeException{

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения при загрузке изображения.
     */
    public ImageUploadException(String message){
        super(message);
    }
}

