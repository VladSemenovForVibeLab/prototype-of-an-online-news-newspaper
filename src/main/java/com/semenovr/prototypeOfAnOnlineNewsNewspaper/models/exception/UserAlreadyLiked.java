package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;


/**
 * Исключение, выбрасываемое в случае, если пользователь уже поставил лайк.
 */
public class UserAlreadyLiked extends RuntimeException {

    /**
     * Создает новое исключение с указанным сообщением.
     *
     * @param s Сообщение об ошибке.
     */
    public UserAlreadyLiked(String s) {
        super(s);
    }
}
