package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое при попытке доступа к статье, которая не существует.
 */
public class ArticleNotFoundException extends RuntimeException{

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения.
     */
    public ArticleNotFoundException(String message){
        super(message);
    }
}
