package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое при попытке добавить "лайк", который уже существует.
 */
public class LikeAlreadyExist extends RuntimeException{

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения при попытке добавить "лайк", который уже существует.
     */
    public LikeAlreadyExist(String message){
        super(message);
    }
}
