package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Map;


/**
 * Класс, представляющий тело исключения, содержащее сообщение и возможные ошибки в виде отображения (Map).
 */
@Data
@AllArgsConstructor
public class ExceptionBody {
    private String message;
    private Map<String,String> errors;

    /**
     * Создает новый объект ExceptionBody с заданным сообщением.
     *
     * @param message Сообщение об ошибке.
     */
    public ExceptionBody(String message) {
        this.message = message;
    }
}

