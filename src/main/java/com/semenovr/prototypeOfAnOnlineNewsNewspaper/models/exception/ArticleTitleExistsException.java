package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое при попытке создать статью с заголовком, который уже существует.
 */
public class ArticleTitleExistsException extends RuntimeException {


    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения.
     */
    public ArticleTitleExistsException(String message) {
        super(message);
    }
}
