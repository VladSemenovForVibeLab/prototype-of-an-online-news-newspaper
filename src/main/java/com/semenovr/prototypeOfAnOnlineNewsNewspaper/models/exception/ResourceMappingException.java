package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception;

/**
 * Исключение, выбрасываемое при возникновении ошибок в процессе маппинга ресурсов.
 */
public class ResourceMappingException extends RuntimeException{

    /**
     * Создает новое исключение с заданным сообщением.
     *
     * @param message Сообщение об ошибке, описывающее причину исключения в контексте маппинга ресурсов.
     */
    public ResourceMappingException(String message) {
        super(message);
    }
}

