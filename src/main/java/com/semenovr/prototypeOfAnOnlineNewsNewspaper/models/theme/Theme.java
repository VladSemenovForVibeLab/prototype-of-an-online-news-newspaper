package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.Set;

/**
 * Класс, представляющий сущность "Тема" (Theme).
 * Этот класс аннотирован как сущность JPA, что позволяет сохранять его в базе данных.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "themes")
public class Theme {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name",nullable = false)
    private String name;

    /**
     * Статьи, связанные с данной темой.
     * Это множество статей, которые привязаны к данной теме через аннотацию @ManyToMany.
     * Аннотация @JsonIgnore предотвращает бесконечную рекурсию при сериализации в JSON, избегая циклических ссылок.
     */
    @JsonIgnore
    @ManyToMany(mappedBy = "themes")
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    private Set<Article> articles;

}

