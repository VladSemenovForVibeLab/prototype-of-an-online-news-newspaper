package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Класс Comment представляет сущность комментария в системе онлайн-газеты.
 * Этот класс аннотирован как сущность JPA и представляет записи в таблице "comments" базы данных.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "comments")
public class Comment {
    // Уникальный идентификатор комментария
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    // Статья, к которой относится данный комментарий
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id")
    private Article article;

    // Автор комментария, связанный с сущностью User
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "author_id")
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private User author;

    // Текст комментария
    @Column(length = 1000)
    private String text;

    // Дата написания комментария
    private LocalDateTime writtenDate;

    // Метод, выполняемый перед сохранением нового комментария
    @PrePersist
    public void init(){
        this.writtenDate=LocalDateTime.now();
    }
}