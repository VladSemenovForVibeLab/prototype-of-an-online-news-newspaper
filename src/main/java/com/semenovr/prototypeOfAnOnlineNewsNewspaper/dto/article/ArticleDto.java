package com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.article;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * Класс ArticleDto представляет собой объект передачи данных (DTO) для статьи.
 * Используется для передачи информации о статье между клиентом и сервером.
 */
@Data
public class ArticleDto {
    private Long id;  // Идентификатор статьи
    private String title;    // Заголовок статьи
    private String text;     // Текст статьи
    private String imageUrl; // URL изображения для статьи
    private Long authorId;   // Идентификатор автора статьи
    private List<Long> commentsIds;// Идентификаторы комментариев к статье
    private Set<Theme> themes; // Набор тем для статьи
}
