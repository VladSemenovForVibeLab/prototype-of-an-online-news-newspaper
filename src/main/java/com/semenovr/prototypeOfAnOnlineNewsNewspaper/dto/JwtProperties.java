package com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Класс JwtProperties представляет собой настройки, связанные с JSON Web Tokens (JWT), используемыми для обеспечения безопасности в приложении.
 * Он помечен как компонент Spring и аннотирован как `@ConfigurationProperties` для привязки свойств из файла конфигурации
 * с указанным префиксом "security.jwt" к полям этого класса.
 */
@Component
@Data
@ConfigurationProperties(prefix = "security.jwt")
public class JwtProperties {
    private String secret;
    private long access;
    private long refresh;
}

