package com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth;


import lombok.Data;

/**
 * Класс JWTResponse представляет собой объект, содержащий информацию о результате успешной аутентификации
 * и генерированных токенах (Access Token и Refresh Token) для пользователя.
 */
@Data
public class JWTResponse {
    private Long id;   // Идентификатор пользователя
    private String username;  // Имя пользователя
    private String accessToken;  // Access Token для аутентифицированного пользователя
    private String refreshToken;  // Refresh Token для обновления Access Token
}

