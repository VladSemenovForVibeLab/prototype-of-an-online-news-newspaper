package com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth;


import lombok.Data;

/**
 * Класс JWTRequest представляет собой объект, содержащий информацию, необходимую для аутентификации
 * и генерации JSON Web Token (JWT) при запросе аутентификации пользователя.
 */
@Data
public class JWTRequest {
    private String username; // Имя пользователя для аутентификации
    private String password; // Пароль пользователя для аутентификации
}

