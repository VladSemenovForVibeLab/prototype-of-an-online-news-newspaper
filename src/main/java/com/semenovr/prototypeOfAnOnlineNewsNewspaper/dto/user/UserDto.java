package com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.user;

import lombok.*;

/**
 * Класс UserDto представляет собой объект передачи данных (DTO) для пользователя.
 * Используется для передачи информации о пользователе между клиентом и сервером.
 */
@Data
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private Long id;  // Идентификатор пользователя
    private String username;   // Имя пользователя
    private String firstname;     // Имя пользователя
    private String lastName;  // Фамилия пользователя
    private String password;  // Пароль пользователя
    private String passwordConfirmation;   // Подтверждение пароля пользователя
}


