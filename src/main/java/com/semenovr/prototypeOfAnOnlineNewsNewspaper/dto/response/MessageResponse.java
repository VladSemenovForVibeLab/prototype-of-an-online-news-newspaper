package com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

/**
 * Класс MessageResponse представляет собой объект, используемый для создания ответа с сообщением и HTTP-статусом.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageResponse {
    private String message;  // Текстовое сообщение
    private HttpStatus status;  // HTTP-статус ответа
}
