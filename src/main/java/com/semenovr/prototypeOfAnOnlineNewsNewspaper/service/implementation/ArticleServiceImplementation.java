package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ArticleTitleExistsException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.CommentRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.LikeRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ArticleService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


/**
 * Класс `ArticleServiceImplementation` представляет собой реализацию сервиса для управления статьями.
 * Этот класс расширяет абстрактный класс `AbstractCRUDService` и реализует интерфейс `ArticleService`.
 * Он отвечает за создание, удаление и получение информации о статьях, а также выполнение связанных операций.
 */
@Service
@RequiredArgsConstructor
public class ArticleServiceImplementation extends AbstractCRUDService<Article,Long> implements ArticleService {
    private final ArticleRepository articleRepository;
    private final CommentRepository commentRepository;
    private final LikeRepository likeRepository;
    @Override
    JpaRepository<Article, Long> getRepository() {
        return articleRepository;
    }


    /**
     * Метод `create` создает новую статью и сохраняет её в базе данных.
     *
     * @param object Новая статья для создания.
     * @throws ArticleTitleExistsException Если статья с указанным заголовком уже существует в базе данных.
     */
    @Override
    @Transactional
    public void create(Article object) {
        String articleTitle = object.getTitle();
        boolean articleExists = checkIfArticleExistsInDatabase(articleTitle);
        if (!articleExists) {
        super.create(object);
        } else {
            throw new ArticleTitleExistsException("Статья с заголовком '\" + articleTitle + \"' уже существует.\"");
        }
    }

    /**
     * Метод `delete` удаляет статью из базы данных и связанные с ней комментарии и лайки.
     *
     * @param object Статья для удаления.
     */
    @Override
    @Transactional
    public void delete(Article object) {
        List<Comment> comments = object.getComments();
        commentRepository.deleteAll(comments);
        List<Like> likes = likeRepository.findByArticle(object);
        likeRepository.deleteAll(likes);
        super.delete(object);
    }

    private boolean checkIfArticleExistsInDatabase(String articleTitle) {
        return articleRepository.existsByTitle(articleTitle);
    }

    /**
     * Метод `getLatestArticles` возвращает список последних статей, опубликованных в течение последних 24 часов.
     *
     * @return Список последних статей, отсортированных по дате публикации в порядке убывания.
     */
    @Override
    @Transactional(readOnly = true)
    public List<Article> getLatestArticles() {
        LocalDateTime twentyFourHoursAgo = LocalDateTime.now().minusHours(24);
        return articleRepository.findByPublicationDateAfterOrderByPublicationDateDesc(twentyFourHoursAgo);
    }
}
