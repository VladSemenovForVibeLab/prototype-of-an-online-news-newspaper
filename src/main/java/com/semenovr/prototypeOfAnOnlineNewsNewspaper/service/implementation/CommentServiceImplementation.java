package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ArticleNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.CommentRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CommentService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Реализация сервиса для работы с комментариями.
 */
@Service
@RequiredArgsConstructor
public class CommentServiceImplementation extends AbstractCRUDService<Comment,Long> implements CommentService {
    private final CommentRepository commentRepository;
    private final ArticleRepository articleRepository;


    /**
     * Переопределение метода для получения репозитория для работы с комментариями.
     *
     * @return Репозиторий для комментариев.
     */
    @Override
    JpaRepository<Comment, Long> getRepository() {
        return commentRepository;
    }

    /**
     * Получение списка комментариев для указанной статьи, сортированных по дате написания в убывающем порядке.
     *
     * @param article Статья, для которой нужно получить комментарии.
     * @return Список комментариев для указанной статьи.
     */
    @Override
    public List<Comment> getCommentsByArticle(Article article) {
        return commentRepository.findByArticleOrderByWrittenDateDesc(article);
    }

    /**
     * Добавление комментария к указанной статье.
     *
     * @param articleId  Идентификатор статьи, к которой нужно добавить комментарий.
     * @param commentDto DTO комментария, содержащий информацию об авторе и тексте.
     * @return Добавленный комментарий.
     */
    @Override
    @Transactional
    public Comment addCommentToArticle(Long articleId, Comment commentDto) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ArticleNotFoundException("Статья не найдена"));
        User author = commentDto.getAuthor();
        String text = commentDto.getText();
        Comment comment = new Comment();
        comment.setAuthor(author);
        comment.setText(text);
        comment.setArticle(article);
        Comment addedComment = commentRepository.save(comment);
        return addedComment;
    }

    /**
     * Удаление комментария из указанной статьи.
     *
     * @param articleId  Идентификатор статьи, из которой нужно удалить комментарий.
     * @param commentId  Идентификатор комментария, который нужно удалить.
     */
    @Override
    @Transactional
    public void deleteCommentFromArticle(Long articleId, Long commentId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ArticleNotFoundException("Статья не найдена"));
        Comment comment = commentRepository.findById(commentId)
                .orElseThrow(() -> new EntityNotFoundException("Comment not found"));
        if (comment.getArticle().equals(article)) {
            commentRepository.delete(comment);
        } else {
            throw new IllegalArgumentException("Комментарий не относится к указанной статье");
        }
    }

    /**
     * Получение списка комментариев для указанной статьи.
     *
     * @param articleId Идентификатор статьи, для которой нужно получить комментарии.
     * @return Список комментариев для указанной статьи.
     */
    public List<Comment> getCommentsForArticle(Long articleId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ArticleNotFoundException("Статья не найдена"));
        return article.getComments();
    }
}
