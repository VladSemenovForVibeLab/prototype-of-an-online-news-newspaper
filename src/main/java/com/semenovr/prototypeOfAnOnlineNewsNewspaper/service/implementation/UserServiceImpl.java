package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.ArticleEditHistory;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ResourceNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleEditHistoryRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Реализация сервиса для работы с пользователями.
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends AbstractCRUDService<User, Long> implements UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final ArticleEditHistoryRepository articleEditHistoryRepository;

    /**
     * Переопределение метода для получения репозитория для работы с пользователями.
     *
     * @return Репозиторий для пользователей.
     */
    @Override
    JpaRepository<User, Long> getRepository() {
        return userRepository;
    }

    /**
     * Метод для создания пользователя.
     *
     * @param object Пользователь, которого нужно создать.
     * @throws IllegalStateException Если пользователь с таким именем уже существует.
     * @throws IllegalStateException Если пароль и подтверждение пароля не совпадают.
     */
    @Override
    @Transactional
    public void create(User object) {
        if (userRepository.findByUsername(object.getUsername()).isPresent()) {
            throw new IllegalStateException("Пользователь уже существует.");
        }
        if (!object.getPassword().equals(object.getPasswordConfirmation())) {
            throw new IllegalStateException("Пароль и подтверждение пароля не совпадают");
        }
        object.setPassword(passwordEncoder.encode(object.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(Role.ROLE_USER);
        object.setRoles(roles);
        userRepository.save(object);
    }

    /**
     * Метод для обновления пользователя.
     *
     * @param object Пользователь, которого нужно обновить.
     * @return Обновленный пользователь.
     */
    @Override
    @Transactional
    public User update(User object) {
        object.setPassword(passwordEncoder.encode(object.getPassword()));
        userRepository.save(object);
        return object;
    }

    /**
     * Метод для получения пользователя по его имени.
     *
     * @param username Имя пользователя.
     * @return Пользователь с указанным именем.
     * @throws ResourceNotFoundException Если пользователь не найден.
     */
    @Override
    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new ResourceNotFoundException("User not found"));
    }

    /**
     * Метод для удаления пользователя.
     *
     * @param object Пользователь, которого нужно удалить.
     */
    @Override
    @Transactional
    public void delete(User object) {
        List<ArticleEditHistory> editHistoryList = articleEditHistoryRepository.findByAdministrator(object);
        List<Article> userArticles = object.getArticles();
        List<Comment> comments = object.getComments();
        for (ArticleEditHistory editHistory : editHistoryList) {
            editHistory.setAdministrator(null); // Устанавливаем administrator в null
        }
        for (Article article : userArticles) {
            article.setAuthor(null);
        }
        for(Comment comment: comments){
            comment.setAuthor(null);
        }
        super.delete(object);
    }
}
