package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;


import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


/**
 * Абстрактный класс AbstractCRUDService представляет собой базовую реализацию
 * интерфейса CRUDService для операций создания, чтения, обновления и удаления сущностей.
 * Этот класс обеспечивает общую логику для сервисов, работающих с сущностями и использующих
 * JPA-репозитории для выполнения операций.
 *
 * @param <E> Тип сущности, с которой работает сервис.
 * @param <K> Тип ключа (идентификатора) сущности.
 */
public abstract class AbstractCRUDService<E,K> implements CRUDService<E,K> {

    /**
     * Абстрактный метод, который должен быть реализован в подклассах, чтобы предоставить
     * JPA-репозиторий, связанный с конкретной сущностью.
     *
     * @return JPA-репозиторий для работы с сущностью.
     */
    abstract JpaRepository<E,K> getRepository();

    @Override
    @Transactional
    public void create(E object){
        getRepository().save(object);
    }
    @Override
    @Transactional(readOnly = true)
    public E findById(K id){
        // Ищем сущность по указанному идентификатору и возвращаем её (или null, если не найдена)
        return getRepository().findById(id).orElse(null);
    }
    @Override
    @Transactional(readOnly = true)
    public List<E> findAll(){
        // Получаем список всех сущностей из репозитория и возвращаем его
        List<E> objects = new ArrayList<>();
        getRepository().findAll().forEach(objects::add);
        return objects;
    }
    @Override
    @Transactional
    public E update(E object){
        // Обновляем (сохраняем) сущность в репозитории и возвращаем её
        getRepository().save(object);
        return object;
    }
    @Override
    @Transactional
    public void delete(E object){
        // Удаляем сущность из репозитория
        getRepository().delete(object);
    }
}

