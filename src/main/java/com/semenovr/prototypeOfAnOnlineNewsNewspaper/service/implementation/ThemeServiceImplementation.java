package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ResourceNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ThemeNameAlreadyExistsException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.UserSettings;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ThemeRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserSettingsRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ThemeService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * Реализация сервиса для работы с темами.
 */
@Service
@RequiredArgsConstructor
public class ThemeServiceImplementation extends AbstractCRUDService<Theme,Long> implements ThemeService {
    private final ThemeRepository themeRepository;
    private final ArticleRepository articleRepository;
    private final UserSettingsRepository userSettingsRepository;

    /**
     * Переопределение метода для получения репозитория для работы с темами.
     *
     * @return Репозиторий для тем.
     */
    @Override
    JpaRepository<Theme, Long> getRepository() {
        return themeRepository;
    }

    /**
     * Метод для создания темы.
     *
     * @param object Тема, которую нужно создать.
     * @throws ThemeNameAlreadyExistsException Если тема с таким именем уже существует.
     */
    @Override
    @Transactional
    public void create(Theme object) {
        String themeName = object.getName();
        // Проверяем существование темы с таким именем
        if (isThemeNameExists(themeName)) {
            // Можете выбрать соответствующее действие, например, выбросить исключение
            throw new ThemeNameAlreadyExistsException("Тема с данным названием '" + themeName + "' уже существует.");
        }
        // Если темы с таким именем нет, сохраняем новую тему
        super.create(object);
    }

    /**
     * Метод для удаления темы.
     *
     * @param object Тема, которую нужно удалить.
     */
    @Override
    @Transactional
    public void delete(Theme object) {
        // Получаем список пользователей с этой темой в избранных
        List<UserSettings> usersWithFavoriteThemes = userSettingsRepository.findUserSettingsByFavoriteThemes(object);
        // Получаем список пользователей с этой темой в запрещенных
        List<UserSettings> usersWithForbiddenThemes = userSettingsRepository.findUserSettingByForbiddenThemes(object);
        // Удаляем записи о теме из списков избранных у всех пользователей
        for (UserSettings userSettings : usersWithFavoriteThemes) {
            userSettings.getFavoriteThemes().remove(object);
        }
        // Удаляем записи о теме из списков запрещенных у всех пользователей
        for (UserSettings userSettings : usersWithForbiddenThemes) {
            userSettings.getForbiddenThemes().remove(object);
        }
        super.delete(object);
    }

    /**
     * Метод для проверки существования темы с указанным именем.
     *
     * @param name Имя темы.
     * @return true, если тема с таким именем существует, в противном случае - false.
     */
    public boolean isThemeNameExists(String name) {
        return themeRepository.existsByName(name);
    }

    /**
     * Метод для привязки темы к статье.
     *
     * @param themeId   Идентификатор темы.
     * @param articleId Идентификатор статьи.
     */
    @Override
    @Transactional
    public void assignThemeToArticle(Long themeId, Long articleId) {
        Theme theme = themeRepository.findById(themeId).orElseThrow(() -> new ResourceNotFoundException("Данной тематики нет"));
        Article article = articleRepository.findById(articleId).orElseThrow(() -> new ResourceNotFoundException("Данной статьи нет"));
        article.getThemes().add(theme);
        articleRepository.save(article);
    }
}
