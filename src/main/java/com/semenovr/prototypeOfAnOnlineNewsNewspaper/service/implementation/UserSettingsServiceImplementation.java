package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.UserSettings;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserSettingsRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserSettingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

/**
 * Реализация сервиса для работы с настройками пользователей.
 */
@Service
@RequiredArgsConstructor
public class UserSettingsServiceImplementation extends AbstractCRUDService<UserSettings,Long> implements UserSettingsService {
    private final UserSettingsRepository userSettingsRepository;

    /**
     * Переопределение метода для получения репозитория для работы с настройками пользователей.
     *
     * @return Репозиторий для настроек пользователей.
     */
    @Override
    JpaRepository<UserSettings, Long> getRepository() {
        return userSettingsRepository;
    }
}
