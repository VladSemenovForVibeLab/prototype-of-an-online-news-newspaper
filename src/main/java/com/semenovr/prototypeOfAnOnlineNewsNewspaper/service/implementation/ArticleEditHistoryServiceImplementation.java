package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.ArticleEditHistory;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.DuplicateCombinationException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ResourceNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleEditHistoryRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ArticleEditHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Класс `ArticleEditHistoryServiceImplementation` представляет собой реализацию сервиса для управления историей редактирования статей.
 * Этот класс расширяет абстрактный класс `AbstractCRUDService` и реализует интерфейс `ArticleEditHistoryService`.
 * Он отвечает за ведение истории изменений статей, а также добавление новых записей в историю.
 */
@Service
@RequiredArgsConstructor
public class ArticleEditHistoryServiceImplementation  extends AbstractCRUDService<ArticleEditHistory,Long> implements ArticleEditHistoryService {
    private final ArticleEditHistoryRepository articleEditHistoryRepository;
    private final ArticleRepository articleRepository;
    private final UserRepository userRepository;
    @Override
    JpaRepository<ArticleEditHistory, Long> getRepository() {
        return articleEditHistoryRepository;
    }

    /**
     * Метод `getEditHistoryForArticle` получает историю редактирования статьи по её идентификатору.
     *
     * @param articleId Идентификатор статьи, для которой нужно получить историю редактирования.
     * @return Список записей истории редактирования статьи, отсортированный по дате редактирования в убывающем порядке.
     * @throws ResourceNotFoundException Если статья с указанным идентификатором не найдена.
     */
    @Override
    @Transactional(readOnly = true)
    public List<ArticleEditHistory> getEditHistoryForArticle(Long articleId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ResourceNotFoundException("Статья не найдена"));
        return articleEditHistoryRepository.findByArticleOrderByEditDateDesc(article);
    }

    /**
     * Метод `addEditHistory` добавляет новую запись в историю редактирования статьи.
     *
     * @param articleId      Идентификатор статьи, для которой нужно добавить запись в историю.
     * @param administratorId Идентификатор администратора, который внес изменения в статью.
     * @throws ResourceNotFoundException       Если статья с указанным идентификатором не найдена.
     * @throws DuplicateCombinationException    Если комбинация articleId и administratorId уже существует в истории редактирования.
     */
    @Override
    @Transactional
    public void addEditHistory(Long articleId, Long administratorId) {
        Article article = articleRepository.findById(articleId)
                .orElseThrow(() -> new ResourceNotFoundException("Статья не найдена"));

        User administrator = userRepository.findById(administratorId)
                .orElseThrow(() -> new ResourceNotFoundException("Пользователь не найден"));

        boolean combinationExists = articleEditHistoryRepository.existsByArticleAndAdministrator(article, administrator);

        if (combinationExists) {
            throw new DuplicateCombinationException("Комбинация articleId и administratorId уже существует");
        }else {
            ArticleEditHistory history = new ArticleEditHistory();
            history.setArticle(article);
            history.setEditDate(LocalDateTime.now());
            history.setAdministrator(administrator);
            articleEditHistoryRepository.save(history);
        }
    }
}
