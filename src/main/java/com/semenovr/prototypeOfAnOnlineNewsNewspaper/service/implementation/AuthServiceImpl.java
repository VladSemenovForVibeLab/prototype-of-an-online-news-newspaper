package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTRequest;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.JwtTokenProvider;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.AuthService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Класс `AuthServiceImpl` представляет собой реализацию сервиса для аутентификации и управления токенами.
 * Он обеспечивает возможность входа пользователя, а также обновление токенов доступа.
 */
@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final JwtTokenProvider jwtTokenProvider;

    /**
     * Метод `login` выполняет процесс аутентификации пользователя на основе предоставленных учетных данных.
     *
     * @param loginRequest Запрос на вход, содержащий имя пользователя и пароль.
     * @return Объект `JWTResponse`, содержащий идентификатор пользователя, имя пользователя и токены доступа и обновления.
     */
    @Override
    @Transactional
    public JWTResponse login(JWTRequest loginRequest) {
        JWTResponse jwtResponse = new JWTResponse();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword()));
        User user = userService.getByUsername(loginRequest.getUsername());
        jwtResponse.setId(user.getId());
        jwtResponse.setUsername(user.getUsername());
        jwtResponse.setAccessToken(jwtTokenProvider.createAccessToken(user.getId(),user.getUsername(),user.getRoles()));
        jwtResponse.setRefreshToken(jwtTokenProvider.createRefreshToken(user.getId(),user.getUsername()));
        return jwtResponse;
    }


    /**
     * Метод `refresh` обновляет токены доступа и обновления пользователя на основе предоставленного токена обновления.
     *
     * @param refreshToken Токен обновления, используемый для получения новых токенов доступа.
     * @return Объект `JWTResponse`, содержащий новые токены доступа и обновления.
     */
    @Override
    @Transactional
    public JWTResponse refresh(String refreshToken) {
        return jwtTokenProvider.refreshUserTokens(refreshToken);
    }
}

