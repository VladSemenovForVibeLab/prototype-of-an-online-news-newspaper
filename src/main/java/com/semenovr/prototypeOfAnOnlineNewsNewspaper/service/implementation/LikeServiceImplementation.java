package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.LikeAlreadyExist;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.LikeNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.UserAlreadyLiked;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.LikeRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.LikeService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Реализация сервиса для работы с лайками.
 */
@Service
@RequiredArgsConstructor
public class LikeServiceImplementation extends AbstractCRUDService<Like,Long> implements LikeService {
    private final LikeRepository likeRepository;

    /**
     * Переопределение метода для получения репозитория для работы с лайками.
     *
     * @return Репозиторий для лайков.
     */
    @Override
    JpaRepository<Like, Long> getRepository() {
        return likeRepository;
    }

    /**
     * Метод для создания лайка к статье.
     *
     * @param object Лайк, который нужно создать.
     * @throws UserAlreadyLiked Если пользователь уже поставил лайк к данной статье.
     */
    @Override
    @Transactional
    public void create(Like object) {
        boolean userAlreadyLiked = checkIfUserLikedArticle(object.getArticle(), object.getUser());
        if (userAlreadyLiked) {
            throw new UserAlreadyLiked("Пользователь уже поставил лайк!");
        } else {
            super.create(object);
        }
    }

    /**
     * Метод для поставки лайка к статье.
     *
     * @param article Статья, которую пользователь лайкает.
     * @param user    Пользователь, который ставит лайк.
     * @return Созданный лайк.
     * @throws LikeAlreadyExist Если пользователь уже поставил лайк к данной статье.
     */
    @Override
    @Transactional
    public Like likeArticle(Article article, User user) {
        // Проверяем, существует ли уже лайк от данного пользователя к данной статье
        if (likeRepository.existsByArticleAndUser(article, user)) {
            // Лайк уже существует, можно выбросить исключение
            throw new LikeAlreadyExist("Этот пользователь уже поставил лайк");
        }
        Like like = new Like();
        like.setArticle(article);
        like.setUser(user);
        return likeRepository.save(like);
    }

    /**
     * Метод для снятия лайка со статьи.
     *
     * @param article Статья, с которой пользователь снимает лайк.
     * @param user    Пользователь, который снимает лайк.
     * @throws LikeNotFoundException Если пользователь не поставил лайк к данной статье.
     */
    @Override
    @Transactional
    public void unlikeArticle(Article article, User user) {
        Like like = likeRepository.findByArticleAndUser(article, user);
        if (like != null) {
            likeRepository.delete(like);
        }else {
            throw new LikeNotFoundException("Пользователь не поставил лайк на данную статью.");
        }
    }

    /**
     * Метод для проверки, поставил ли пользователь лайк к статье.
     *
     * @param article Статья, которую пользователь может лайкнуть.
     * @param user    Пользователь, который может поставить лайк.
     * @return true, если пользователь поставил лайк к статье, в противном случае - false.
     */
    @Override
    @Transactional(readOnly = true)
    public boolean hasUserLikedArticle(Article article, User user) {
        return likeRepository.existsByArticleAndUser(article, user);
    }
    private boolean checkIfUserLikedArticle(Article article, User user) {
        Like existingLike = likeRepository.findByArticleAndUser(article, user);
        return existingLike != null;
    }
}
