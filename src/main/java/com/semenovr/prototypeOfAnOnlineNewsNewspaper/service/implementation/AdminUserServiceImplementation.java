package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.UserNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.AdminUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * Класс AdminUserServiceImplementation представляет собой реализацию интерфейса AdminUserService.
 * Он обеспечивает функционал администратора для управления ролями пользователей в системе.
 */
@Service
@RequiredArgsConstructor
public class AdminUserServiceImplementation implements AdminUserService {
    private final UserRepository userRepository;

    /**
     * Метод assignAdminRole назначает роль администратора указанному пользователю.
     *
     * @param userId Идентификатор пользователя, которому назначается роль администратора.
     * @throws UserNotFoundException если пользователь с указанным идентификатором не найден.
     */
    @Override
    @Transactional
    public void assignAdminRole(Long userId) {
        // Находим пользователя по его идентификатору или выбрасываем исключение, если пользователь не найден
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new UserNotFoundException("Пользователь не найден"));
        // Получаем текущие роли пользователя и добавляем к ним роль администратора
        Set<Role> roles = user.getRoles();
        roles.add(Role.ROLE_ADMIN);
        // Устанавливаем обновленный набор ролей пользователю и сохраняем изменения в репозитории
        user.setRoles(roles);
        userRepository.save(user);
    }
}
