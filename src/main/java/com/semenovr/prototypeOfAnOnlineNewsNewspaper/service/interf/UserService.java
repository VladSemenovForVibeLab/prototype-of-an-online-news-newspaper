package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;

/**
 * Интерфейс UserService предоставляет методы для управления пользователями.
 * Расширяет CRUDService, определяя операции создания, поиска, обновления и удаления пользователей.
 * Параметры типов User и Long обозначают тип пользователя и тип идентификатора соответственно.
 */
public interface UserService extends CRUDService<User,Long>{

    /**
     * Метод для получения пользователя по его имени пользователя (username).
     *
     * @param username Имя пользователя, по которому выполняется поиск.
     * @return Объект пользователя с указанным именем пользователя или null, если не найден.
     */
    User getByUsername(String username);

}
