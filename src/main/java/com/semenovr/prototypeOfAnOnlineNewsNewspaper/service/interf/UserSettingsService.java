package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.UserSettings;

/**
 * Интерфейс UserSettingsService предоставляет методы для управления настройками пользователей.
 * Расширяет CRUDService, определяя операции создания, поиска, обновления и удаления настроек пользователя.
 * Параметры типов UserSettings и Long обозначают тип настроек пользователя и тип идентификатора соответственно.
 */
public interface UserSettingsService extends CRUDService<UserSettings,Long> {
}
