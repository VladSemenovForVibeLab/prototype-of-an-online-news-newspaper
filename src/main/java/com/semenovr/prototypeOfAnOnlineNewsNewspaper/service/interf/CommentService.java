package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;

import java.util.List;

/**
 * Интерфейс для службы управления комментариями.
 */
public interface CommentService extends CRUDService<Comment,Long>{

    /**
     * Метод для получения комментариев для указанной статьи.
     *
     * @param article Статья, для которой нужно получить комментарии.
     * @return Список комментариев для указанной статьи.
     */
    List<Comment> getCommentsByArticle(Article article);

    /**
     * Метод для добавления комментария к указанной статье.
     *
     * @param articleId  Идентификатор статьи, к которой нужно добавить комментарий.
     * @param commentDto Комментарий, который нужно добавить.
     * @return Добавленный комментарий.
     */
    Comment addCommentToArticle(Long articleId, Comment commentDto);

    /**
     * Метод для удаления комментария из указанной статьи.
     *
     * @param articleId Идентификатор статьи, из которой нужно удалить комментарий.
     * @param commentId Идентификатор комментария, который нужно удалить.
     */
    void deleteCommentFromArticle(Long articleId, Long commentId);
}
