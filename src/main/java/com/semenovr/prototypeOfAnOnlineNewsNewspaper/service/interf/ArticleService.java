package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;

import java.util.List;

/**
 * Интерфейс для службы управления статьями.
 */
public interface ArticleService extends CRUDService<Article,Long> {

    /**
     * Метод для получения последних статей.
     *
     * @return Список последних статей.
     */
    List<Article> getLatestArticles();
}
