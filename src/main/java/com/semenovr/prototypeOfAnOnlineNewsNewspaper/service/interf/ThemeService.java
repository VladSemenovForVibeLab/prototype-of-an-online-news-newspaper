package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;

/**
 * Интерфейс ThemeService предоставляет методы для управления темами и их назначением к статьям.
 * Расширяет CRUDService, определяя операции создания, поиска, обновления и удаления тем.
 * Параметры типов Theme и Long обозначают тип темы и тип идентификатора соответственно.
 */
public interface ThemeService extends CRUDService<Theme,Long> {

    /**
     * Метод для назначения темы к указанной статье.
     *
     * @param themeId Идентификатор темы, которую необходимо назначить.
     * @param articleId Идентификатор статьи, к которой необходимо назначить тему.
     */
    void assignThemeToArticle(Long themeId, Long articleId);
}
