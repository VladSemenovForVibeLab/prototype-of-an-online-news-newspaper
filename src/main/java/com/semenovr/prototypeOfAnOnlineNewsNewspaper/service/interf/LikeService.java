package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;

/**
 * Интерфейс LikeService предоставляет методы для управления лайками к статьям.
 * Расширяет CRUDService, определяя операции создания, поиска, обновления и удаления лайков.
 * Параметры типов Like и Long обозначают тип лайка и тип идентификатора соответственно.
 */
public interface LikeService extends CRUDService<Like,Long>{

    /**
     * Метод для добавления лайка к статье от указанного пользователя.
     *
     * @param article Статья, к которой добавляется лайк.
     * @param user Пользователь, добавляющий лайк.
     * @return Объект лайка, представляющий созданный лайк.
     */
    Like likeArticle(Article article, User user);

    /**
     * Метод для удаления лайка от указанного пользователя к статье.
     *
     * @param article Статья, к которой отменяется лайк.
     * @param user Пользователь, отменяющий лайк.
     */
    void unlikeArticle(Article article, User user);

    /**
     * Метод для проверки, поставил ли указанный пользователь лайк к статье.
     *
     * @param article Статья, к которой проверяется наличие лайка.
     * @param user Пользователь, для которого проверяется наличие лайка.
     * @return true, если пользователь поставил лайк, в противном случае false.
     */
    boolean hasUserLikedArticle(Article article, User user);
}
