package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTRequest;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTResponse;

/**
 * Интерфейс для службы аутентификации.
 */
public interface AuthService {

    /**
     * Метод для выполнения входа пользователя.
     *
     * @param loginRequest Запрос на вход пользователя.
     * @return Ответ с JWT-токенами для пользователя.
     */
    JWTResponse login(JWTRequest loginRequest);


    /**
     * Метод для обновления JWT-токена с использованием Refresh-токена.
     *
     * @param refreshToken Refresh-токен, необходимый для обновления JWT-токена.
     * @return Ответ с обновленным JWT-токеном.
     */
    JWTResponse refresh(String refreshToken);
}

