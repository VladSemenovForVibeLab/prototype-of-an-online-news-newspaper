package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.ArticleEditHistory;

import java.util.List;

/**
 * Интерфейс для службы управления историей редактирования статей.
 */
public interface ArticleEditHistoryService extends CRUDService<ArticleEditHistory,Long>{

    /**
     * Метод для получения истории редактирования для указанной статьи.
     *
     * @param articleId Идентификатор статьи, для которой нужно получить историю редактирования.
     * @return Список записей истории редактирования для статьи.
     */
    List<ArticleEditHistory> getEditHistoryForArticle(Long articleId);


    /**
     * Метод для добавления записи истории редактирования для указанной статьи и администратора.
     *
     * @param articleId      Идентификатор статьи, для которой добавляется запись истории редактирования.
     * @param administratorId Идентификатор администратора, который внес изменения.
     */
    void addEditHistory(Long articleId, Long administratorId);
}
