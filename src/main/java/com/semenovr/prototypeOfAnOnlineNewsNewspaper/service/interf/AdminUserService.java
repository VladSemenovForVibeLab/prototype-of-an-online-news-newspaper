package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf;

/**
 * Интерфейс для службы управления администраторами.
 */
public interface AdminUserService {

    /**
     * Метод для назначения пользователю роли администратора.
     *
     * @param userId Идентификатор пользователя, которому нужно назначить роль администратора.
     */
    void assignAdminRole(Long userId);
}
