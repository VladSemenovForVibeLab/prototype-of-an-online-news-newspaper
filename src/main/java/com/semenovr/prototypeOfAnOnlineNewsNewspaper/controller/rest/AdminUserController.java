package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.AdminUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Контроллер REST API для управления администраторскими ролями пользователей.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(AdminUserController.ADMIN_USER_REST_URL)
public class AdminUserController {
    public static final String ADMIN_USER_REST_URL = "/api/v1/admins";

    private final AdminUserService adminUserService;

    /**
     * Присваивает пользователю администраторскую роль по его идентификатору.
     *
     * @param userId Идентификатор пользователя, которому необходимо присвоить администраторскую роль.
     * @return Ответ с кодом статуса HTTP.
     */
    @PostMapping("/{userId}/assign-admin-role")
    public ResponseEntity<Void> assignAdminRole(@PathVariable Long userId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                adminUserService.assignAdminRole(userId);
                return ResponseEntity.ok().build();
            } else {
                // Возвращаем статус NOT_ACCEPTABLE, если текущий пользователь не имеет прав администратора.
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        // Возвращаем статус BAD_REQUEST, если аутентификация не выполнена.
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
