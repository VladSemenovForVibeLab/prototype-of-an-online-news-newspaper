package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Этот класс представляет собой REST-контроллер для управления пользователями и связанными с ними операциями.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(UserRestController.USER_REST_URL)
public class UserRestController extends CRUDRestController<User, Long> {
    // Корневой URL для REST-контроллера пользователя.
    public static final String USER_REST_URL = "/api/v1/user";

    private final UserService userService;

    @Override
    CRUDService<User, Long> getService() {
        return userService;
    }

    /**
     * Обрабатывает POST-запрос для создания нового пользователя.
     *
     * @param object Пользователь для создания.
     * @return ResponseEntity с созданным пользователем или ошибкой.
     */
    @Override
    @PostMapping
    public ResponseEntity<User> create(@RequestBody User object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                getService().create(object);
                return ResponseEntity.ok(object);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Обрабатывает GET-запрос для поиска пользователя по ID.
     *
     * @param id ID пользователя для поиска.
     * @return ResponseEntity с найденным пользователем или ошибкой.
     */
    @Override
    @GetMapping("/{id}")
    public ResponseEntity<User> findById(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                User object = getService().findById(id);
                if (object == null) {
                    return ResponseEntity.notFound().build();
                }
                return ResponseEntity.ok(object);
            } else {
                Long userId = getUserIdFromAuthentication(authentication);
                if (id.equals(userId)) {
                    User object = getService().findById(id);
                    if (object == null) {
                        return ResponseEntity.notFound().build();
                    }
                    return ResponseEntity.ok(object);
                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Обрабатывает GET-запрос для поиска всех пользователей.
     *
     * @return ResponseEntity со списком пользователей или ошибкой.
     */
    @Override
    public ResponseEntity<List<User>> findAll() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                return super.findAll();
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Обрабатывает PUT-запрос для обновления информации о пользователе.
     *
     * @param object Пользователь с обновленными данными.
     * @return ResponseEntity с обновленным пользователем или ошибкой.
     */
    @Override
    @PutMapping
    public ResponseEntity<User> update(@RequestBody User object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                return ResponseEntity.ok().body(userService.update(object));
            } else {
                Long userId = getUserIdFromAuthentication(authentication);
                Long objectId = object.getId();
                if (objectId.equals(userId)) {
                    User objectFind = userService.findById(objectId);
                    if (objectFind == null) {
                        return ResponseEntity.notFound().build();
                    }
                    userService.update(objectFind);
                    return ResponseEntity.ok(objectFind);
                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Обрабатывает DELETE-запрос для удаления пользователя по ID.
     *
     * @param id ID пользователя для удаления.
     * @return ResponseEntity с успешным статусом или ошибкой.
     */
    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                User objectForDelete = getService().findById(id);
                getService().delete(objectForDelete);
                return ResponseEntity.noContent().build();
            } else {
                Long userId = getUserIdFromAuthentication(authentication);
                if (id.equals(userId)) {
                    User objectForDelete = getService().findById(id);
                    if (objectForDelete == null) {
                        return ResponseEntity.notFound().build();
                    }
                    getService().delete(objectForDelete);
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Получает ID пользователя из объекта аутентификации.
     *
     * @param authentication Объект аутентификации пользователя.
     * @return ID пользователя.
     */
    private Long getUserIdFromAuthentication(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userService.getByUsername(userDetails.getUsername());
        return user.getId();
    }
}

