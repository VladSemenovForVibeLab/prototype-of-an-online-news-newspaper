package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.response.MessageResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.LikeNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.UserAlreadyLiked;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Контроллер для управления лайками статей и выполнения связанных операций.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(LikeRestController.LIKE_REST_URL)
public class LikeRestController extends CRUDRestController<Like, Long> {
    private final LikeService likeService;
    private final ArticleService articleService;
    private final UserService userService;
    public static final String LIKE_REST_URL = "/api/v1/likes";

    @Override
    CRUDService<Like, Long> getService() {
        return likeService;
    }

    /**
     * Метод для создания лайка к статье. Проверяет роль пользователя и выполняет действие соответственно.
     * @param object Like-объект, который нужно создать.
     * @return ResponseEntity с созданным объектом или статусом ошибки.
     */
    @Override
    @PostMapping
    public ResponseEntity<Like> create(@RequestBody Like object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                try {
                    likeService.create(object);
                    return ResponseEntity.ok(object);
                } catch (UserAlreadyLiked e) {
                    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                            .build();
                }
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод для получения списка всех лайков. Проверяет роль пользователя и возвращает список лайков или статус ошибки.
     * @return ResponseEntity со списком лайков или статусом ошибки.
     */
    @Override
    @GetMapping
    public ResponseEntity<List<Like>> findAll() {
        List<Like> objects = getService().findAll();
        return ResponseEntity.ok(objects);
    }

    /**
     * Метод для обновления лайка. Проверяет роль пользователя и выполняет действие соответственно.
     * @param object Like-объект, который нужно обновить.
     * @return ResponseEntity с обновленным объектом или статусом ошибки.
     */
    @Override
    @PutMapping
    public ResponseEntity<Like> update(@RequestBody Like object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                Like updatedObject = getService().update(object);
                return ResponseEntity.ok(updatedObject);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод для удаления лайка по идентификатору. Проверяет роль пользователя и выполняет действие соответственно.
     * @param id Идентификатор лайка для удаления.
     * @return ResponseEntity с результатом удаления или статусом ошибки.
     */
    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            Like objectForDelete = getService().findById(id);
            if (isAdmin) {
                getService().delete(objectForDelete);
                return ResponseEntity.noContent().build();
            } else {
                Long userIdAuthentication = getUserIdFromAuthentication(authentication);
                if (userIdAuthentication.equals(objectForDelete.getUser().getId())) {
                    getService().delete(objectForDelete);
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Метод для создания лайка к статье. Проверяет роль пользователя и выполняет действие соответственно.
     * @param articleId Идентификатор статьи.
     * @param userId Идентификатор пользователя.
     * @return ResponseEntity с сообщением о результате операции.
     */
    @PostMapping("/like")
    public ResponseEntity<?> likeArticle(@RequestParam Long articleId, @RequestParam Long userId) {
        Article article = articleService.findById(articleId);
        User user = userService.findById(userId);
        if (article != null && user != null) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            // Проверьте роль пользователя
            if (authentication != null) {
                boolean isAdmin = authentication.getAuthorities().stream()
                        .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
                if (isAdmin) {
                    Like like = likeService.likeArticle(article, user);
                    return ResponseEntity.ok(new MessageResponse("Лайк успешно поставлен!", HttpStatus.OK));
                } else {
                    Long userIdAuthentication = getUserIdFromAuthentication(authentication);
                    if (userIdAuthentication.equals(userId)) {
                        Like like = likeService.likeArticle(article, user);
                        return ResponseEntity.ok(new MessageResponse("Лайк успешно поставлен!", HttpStatus.OK));
                    } else {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }
                }
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод для снятия лайка с статьи. Проверяет роль пользователя и выполняет действие соответственно.
     * @param articleId Идентификатор статьи.
     * @param userId Идентификатор пользователя.
     * @return ResponseEntity с сообщением о результате операции.
     */
    @PostMapping("/unlike")
    public ResponseEntity<?> unlikeArticle(@RequestParam Long articleId, @RequestParam Long userId) {
        Article article = articleService.findById(articleId);
        User user = userService.findById(userId);
        if (article != null && user != null) {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            // Проверьте роль пользователя
            if (authentication != null) {
                boolean isAdmin = authentication.getAuthorities().stream()
                        .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
                if (isAdmin) {
                    try {
                        likeService.unlikeArticle(article, user);
                        return ResponseEntity.ok(new MessageResponse("Лайк был успешно убран!", HttpStatus.OK));
                    } catch (LikeNotFoundException e) {
                        return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                                .body(new MessageResponse(e.getMessage(), HttpStatus.BAD_REQUEST));
                    }
                } else {
                    Long userIdAuthentication = getUserIdFromAuthentication(authentication);
                    if (userIdAuthentication.equals(userId)) {
                        try {
                            likeService.unlikeArticle(article, user);
                            return ResponseEntity.ok(new MessageResponse("Вы успешно убрали свой лайк!", HttpStatus.OK));
                        } catch (LikeNotFoundException e) {
                            return ResponseEntity.status(HttpStatus.BAD_REQUEST)
                                    .body(new MessageResponse(e.getMessage(), HttpStatus.BAD_REQUEST));
                        }
                    } else {
                        return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                    }
                }
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод для проверки, поставил ли пользователь лайк к статье.
     * @param articleId Идентификатор статьи.
     * @param userId Идентификатор пользователя.
     * @return ResponseEntity с сообщением о результате операции.
     */
    @GetMapping("/hasLiked")
    public ResponseEntity<?> hasUserLikedArticle(@RequestParam Long articleId, @RequestParam Long userId) {
        Article article = articleService.findById(articleId);
        User user = userService.findById(userId);
        if (article != null && user != null) {
            boolean hasLiked = likeService.hasUserLikedArticle(article, user);
            return ResponseEntity.ok(new MessageResponse("Вы хотите проверить имеется ли лайк человека на этой статье? -> " + hasLiked, HttpStatus.OK));
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }


    private Long getUserIdFromAuthentication(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userService.getByUsername(userDetails.getUsername());
        return user.getId();
    }
}
