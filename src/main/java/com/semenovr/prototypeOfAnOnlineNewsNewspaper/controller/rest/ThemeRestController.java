package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.LikeService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ThemeService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

/**
 * Контроллер для управления темами новостных статей и выполнения связанных операций.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(ThemeRestController.THEME_REST_URL)
public class ThemeRestController extends CRUDRestController<Theme, Long> {
    private final ThemeService themeService;
    public static final String THEME_REST_URL = "/api/v1/themes";

    @Override
    CRUDService<Theme, Long> getService() {
        return themeService;
    }

    /**
     * Метод для создания новой темы. Проверяет роль пользователя и выполняет действие соответственно.
     * @param object Theme-объект, который нужно создать.
     * @return ResponseEntity с созданным объектом или статусом ошибки.
     */
    @Override
    @PostMapping
    public ResponseEntity<Theme> create(@RequestBody Theme object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                getService().create(object);
                return ResponseEntity.ok(object);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Метод для обновления информации о теме. Проверяет роль пользователя и выполняет действие соответственно.
     * @param object Theme-объект, который нужно обновить.
     * @return ResponseEntity с обновленным объектом или статусом ошибки.
     */
    @Override
    @PutMapping
    public ResponseEntity<Theme> update(@RequestBody Theme object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                Theme updatedObject = themeService.update(object);
                return ResponseEntity.ok(updatedObject);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }

        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Метод для удаления темы по идентификатору. Проверяет роль пользователя и выполняет действие соответственно.
     * @param id Идентификатор темы для удаления.
     * @return ResponseEntity с результатом удаления или статусом ошибки.
     */
    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                Theme objectForDelete = getService().findById(id);
                getService().delete(objectForDelete);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
    }

    /**
     * Метод для назначения темы к статье. Проверяет роль пользователя и выполняет действие соответственно.
     * @param themeId Идентификатор темы.
     * @param articleId Идентификатор статьи.
     * @return ResponseEntity с результатом операции или статусом ошибки.
     */
    @PostMapping("/{themeId}/articles/{articleId}")
    public ResponseEntity<Void> assignThemeToArticle(@PathVariable Long themeId, @PathVariable Long articleId) {
        themeService.assignThemeToArticle(themeId, articleId);
        return ResponseEntity.noContent().build();
    }
}
