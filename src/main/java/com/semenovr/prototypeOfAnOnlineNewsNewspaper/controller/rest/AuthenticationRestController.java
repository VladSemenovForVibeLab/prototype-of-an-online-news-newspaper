package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTRequest;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.response.MessageResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.user.UserDto;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.mappers.UserMapper;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.AuthService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Контроллер REST API для аутентификации и авторизации пользователей.
 */
@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
public class AuthenticationRestController {
    private final AuthService authService;
    private final UserService userService;
    private final UserMapper userMapper;

    /**
     * Метод для входа пользователя в систему.
     *
     * @param loginRequest Запрос на вход с учетными данными пользователя.
     * @return JWTResponse с токеном доступа и информацией о пользователе.
     */
    @PostMapping("/login")
    public JWTResponse login(@RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }

    /**
     * Метод для регистрации нового пользователя.
     *
     * @param userDto DTO пользователя с данными для регистрации.
     * @return ResponseEntity с сообщением о результате операции и статусом.
     */
    @PostMapping("/register")
    public ResponseEntity<MessageResponse> register(@RequestBody UserDto userDto){
        User user = userMapper.toEntity(userDto);
        userService.create(user);
        return new ResponseEntity<>(new MessageResponse("Пользователь сохранён успешно", HttpStatus.CREATED),HttpStatus.OK);
    }

    /**
     * Метод для обновления токена доступа с использованием refresh-токена.
     *
     * @param refreshToken Refresh-токен для обновления токена доступа.
     * @return JWTResponse с новым токеном доступа и информацией о пользователе.
     */
    @PostMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }
}
