package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.response.MessageResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.ArticleEditHistory;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.DuplicateCombinationException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ArticleEditHistoryService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ArticleService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

/**
 * Контроллер REST API для управления историей редактирования статей.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(ArticleEditHistoryRestController.ARTICLE_EDIT_REST_URL)
public class ArticleEditHistoryRestController extends CRUDRestController<ArticleEditHistory, Long> {
    public static final String ARTICLE_EDIT_REST_URL = "/api/v1/articleedits";
    private final ArticleEditHistoryService articleEditHistoryService;

    // Метод getService() переопределен для предоставления сервиса управления историей редактирования статей.
    @Override
    CRUDService<ArticleEditHistory, Long> getService() {
        return articleEditHistoryService;
    }

    /**
     * Метод создания записи истории редактирования статьи.
     *
     * @param object Запись истории редактирования для создания.
     * @return ResponseEntity с созданной записью истории редактирования или статусом ошибки.
     */
    @Override
    @PostMapping
    public ResponseEntity<ArticleEditHistory> create(@RequestBody ArticleEditHistory object) {
        // Получение текущей аутентификации пользователя.
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                // Вызов сервиса для создания записи истории редактирования.
                getService().create(object);
                return ResponseEntity.ok(object);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод получения записи истории редактирования статьи по её идентификатору.
     *
     * @param id Идентификатор записи истории редактирования.
     * @return ResponseEntity с найденной записью истории редактирования или статусом ошибки.
     */
    @Override
    @GetMapping("/{id}")
    public ResponseEntity<ArticleEditHistory> findById(@PathVariable Long id) {
        // Получение текущей аутентификации пользователя.
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                // Вызов сервиса для поиска записи истории редактирования по идентификатору.
                ArticleEditHistory object = getService().findById(id);
                if (object == null) {
                    return ResponseEntity.notFound().build();
                }
                return ResponseEntity.ok(object);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод получения списка всех записей истории редактирования статей.
     *
     * @return ResponseEntity со списком записей истории редактирования или статусом ошибки.
     */
    @Override
    @GetMapping
    public ResponseEntity<List<ArticleEditHistory>> findAll() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                List<ArticleEditHistory> objects = getService().findAll();
                return ResponseEntity.ok(objects);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод обновления записи истории редактирования статьи.
     *
     * @param object Обновленная запись истории редактирования.
     * @return ResponseEntity с обновленной записью истории редактирования или статусом ошибки.
     */
    @Override
    @PutMapping
    public ResponseEntity<ArticleEditHistory> update(@RequestBody ArticleEditHistory object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                ArticleEditHistory updatedObject = getService().update(object);
                return ResponseEntity.ok(updatedObject);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод удаления записи истории редактирования статьи по идентификатору.
     *
     * @param id Идентификатор записи истории редактирования.
     * @return ResponseEntity со статусом удаления или статусом ошибки.
     */
    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                ArticleEditHistory objectForDelete = getService().findById(id);
                getService().delete(objectForDelete);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Добавление истории редактирования статьи.
     *
     * @param articleId       Идентификатор статьи.
     * @param administratorId Идентификатор администратора.
     * @return ResponseEntity с сообщением об успешном добавлении или статусом ошибки.
     */
    // Добавление истории редактирования статьи
    @PostMapping("/add")
    public ResponseEntity<MessageResponse> addEditHistory(@RequestParam Long articleId, @RequestParam Long administratorId) {
        // Получение текущей аутентификации пользователя.
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                try {
                    // Вызов сервиса для добавления истории редактирования статьи.
                    articleEditHistoryService.addEditHistory(articleId, administratorId);
                    return ResponseEntity.ok(new MessageResponse("История редактирования успешно добавлена", HttpStatus.OK));
                } catch (DuplicateCombinationException e) {
                    return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(new MessageResponse("Ошибка при добавлении истории редактирования", HttpStatus.BAD_REQUEST));
                }
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Получение истории редактирования для конкретной статьи по её идентификатору.
     *
     * @param articleId Идентификатор статьи.
     * @return ResponseEntity с историей редактирования или статусом ошибки.
     */
    // Получение истории редактирования для статьи
    @GetMapping("/article/{articleId}")
    public ResponseEntity<List<ArticleEditHistory>> getEditHistoryForArticle(@PathVariable Long articleId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                List<ArticleEditHistory> editHistory = articleEditHistoryService.getEditHistoryForArticle(articleId);
                return ResponseEntity.ok(editHistory);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }
}
