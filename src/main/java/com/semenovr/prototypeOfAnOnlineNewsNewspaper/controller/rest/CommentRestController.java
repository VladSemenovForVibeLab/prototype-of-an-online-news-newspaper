package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CommentService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Контроллер REST API для управления комментариями.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(CommentRestController.COMMENT_REST_URL)
public class CommentRestController extends CRUDRestController<Comment, Long> {
    private final CommentService commentService;
    private final UserService userService;
    public static final String COMMENT_REST_URL = "/api/v1/comments";

    @Override
    CRUDService<Comment, Long> getService() {
        return commentService;
    }

    /**
     * Метод обновления комментария.
     *
     * @param object Обновленный комментарий.
     * @return ResponseEntity с обновленным комментарием или статусом ошибки.
     */
    @Override
    @PutMapping
    public ResponseEntity<Comment> update(@RequestBody Comment object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                Comment updatedObject = commentService.update(object);
                return ResponseEntity.ok(updatedObject);
            } else {
                Long userIdAuthentication = getUserIdFromAuthentication(authentication);
                User author = object.getAuthor();
                if (userIdAuthentication.equals(author.getId())) {
                    Comment updatedObject = commentService.update(object);
                    return ResponseEntity.ok(updatedObject);
                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Метод удаления комментария по идентификатору.
     *
     * @param id Идентификатор комментария.
     * @return ResponseEntity с результатом удаления или статусом ошибки.
     */
    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            Comment objectForDelete = getService().findById(id);
            if (isAdmin) {
                getService().delete(objectForDelete);
                return ResponseEntity.noContent().build();
            } else {
                Long userIdAuthentication = getUserIdFromAuthentication(authentication);
                if (userIdAuthentication.equals(objectForDelete.getAuthor().getId())) {
                    getService().delete(objectForDelete);
                    return ResponseEntity.noContent().build();
                } else {
                    return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Метод для получения комментариев к статье.
     *
     * @param article Статья, к которой запрашиваются комментарии.
     * @return ResponseEntity со списком комментариев или статусом ошибки.
     */
    @GetMapping("/article")
    public ResponseEntity<?> getCommentsByArticle(@RequestBody Article article) {
        List<Comment> comments = commentService.getCommentsByArticle(article);
        return ResponseEntity.ok(comments);
    }

    /**
     * Метод для добавления комментария к статье.
     *
     * @param articleId Идентификатор статьи, к которой добавляется комментарий.
     * @param commentDto DTO нового комментария.
     * @return ResponseEntity с добавленным комментарием или статусом ошибки.
     */
    @PostMapping("/{articleId}")
    public ResponseEntity<Comment> addCommentToArticle(@PathVariable Long articleId, @RequestBody Comment commentDto) {
        Comment addedComment = commentService.addCommentToArticle(articleId, commentDto);
        return ResponseEntity.ok(addedComment);
    }

    /**
     * Метод для удаления комментария из статьи.
     *
     * @param articleId Идентификатор статьи, из которой удаляется комментарий.
     * @param commentId Идентификатор комментария для удаления.
     * @return ResponseEntity с результатом удаления или статусом ошибки.
     */
    @DeleteMapping("/{articleId}/comment-id/{commentId}")
    public ResponseEntity<Void> deleteCommentFromArticle(@PathVariable Long articleId, @PathVariable Long commentId) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                try {
                    commentService.deleteCommentFromArticle(articleId, commentId);
                    return ResponseEntity.noContent().build();
                } catch (IllegalArgumentException e) {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            } else {
                Long userIdAuthentication = getUserIdFromAuthentication(authentication);
                Long commentAuthorId = commentService.findById(commentId).getAuthor().getId();
                if (userIdAuthentication.equals(commentAuthorId)) {
                    try {
                        commentService.deleteCommentFromArticle(articleId, commentId);
                        return ResponseEntity.noContent().build();
                    } catch (IllegalArgumentException e) {
                        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                    }
                } else {
                    return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
                }
            }
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    /**
     * Метод для получения идентификатора пользователя из аутентификации.
     *
     * @param authentication Объект аутентификации.
     * @return Идентификатор пользователя.
     */
    private Long getUserIdFromAuthentication(Authentication authentication) {
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        User user = userService.getByUsername(userDetails.getUsername());
        return user.getId();
    }
}
