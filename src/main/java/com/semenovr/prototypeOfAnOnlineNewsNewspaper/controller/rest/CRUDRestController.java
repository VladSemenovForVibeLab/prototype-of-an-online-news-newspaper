package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Абстрактный базовый класс для REST-контроллеров с основными CRUD-операциями.
 * @param <E> Тип сущности (Entity).
 * @param <K> Тип идентификатора (Key) сущности.
 */
public abstract class CRUDRestController<E,K> {

    /**
     * Абстрактный метод, который должен быть реализован в подклассах для предоставления сервиса для сущности.
     * @return Сервис для работы с сущностью.
     */
    abstract CRUDService<E,K> getService();

    /**
     * Метод для создания новой сущности.
     * @param object Объект, который нужно создать.
     * @return ResponseEntity с созданным объектом или статусом ошибки.
     */
    @PostMapping
    public ResponseEntity<E> create(@RequestBody E object){
        getService().create(object);
        return ResponseEntity.ok(object);
    }

    /**
     * Метод для поиска сущности по идентификатору.
     * @param id Идентификатор сущности.
     * @return ResponseEntity с найденной сущностью или статусом NOT_FOUND, если сущность не найдена.
     */
    @GetMapping("/{id}")
    public ResponseEntity<E> findById(@PathVariable K id){
        E object = getService().findById(id);
        if(object==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(object);
    }

    /**
     * Метод для получения списка всех сущностей.
     * @return ResponseEntity со списком сущностей или статусом ошибки.
     */
    @GetMapping
    public ResponseEntity<List<E>> findAll(){
        List<E> objects = getService().findAll();
        return ResponseEntity.ok(objects);
    }

    /**
     * Метод для обновления существующей сущности.
     * @param object Объект, который нужно обновить.
     * @return ResponseEntity с обновленной сущностью или статусом ошибки.
     */
    @PutMapping
    public ResponseEntity<E> update(@RequestBody E object){
        E updatedObject = getService().update(object);
        return ResponseEntity.ok(updatedObject);
    }

    /**
     * Метод для удаления сущности по идентификатору.
     * @param id Идентификатор сущности для удаления.
     * @return ResponseEntity с результатом удаления или статусом ошибки.
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable K id){
        E objectForDelete = getService().findById(id);
        getService().delete(objectForDelete);
        return ResponseEntity.noContent().build();
    }
}

