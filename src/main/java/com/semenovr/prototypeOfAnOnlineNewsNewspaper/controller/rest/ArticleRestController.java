package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ArticleTitleExistsException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ArticleService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Контроллер REST API для управления статьями.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(ArticleRestController.ARTICLE_REST_URL)
public class ArticleRestController extends CRUDRestController<Article, Long> {
    private final ArticleService articleService;
    private final UserService userService;
    public static final String ARTICLE_REST_URL = "/api/v1/articles";

    @Override
    CRUDService<Article, Long> getService() {
        return articleService;
    }

    /**
     * Метод создания статьи.
     *
     * @param object Статья для создания.
     * @return ResponseEntity с созданной статьей или статусом ошибки.
     */
    @Override
    @PostMapping
    public ResponseEntity<Article> create(@RequestBody Article object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                try {
                    getService().create(object);
                    return ResponseEntity.ok(object);
                } catch (ArticleTitleExistsException e) {
                    return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
                }
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод обновления статьи.
     *
     * @param object Обновленная статья.
     * @return ResponseEntity с обновленной статьей или статусом ошибки.
     */
    @Override
    public ResponseEntity<Article> update(Article object) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                Article updatedObject = getService().update(object);
                return ResponseEntity.ok(updatedObject);
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод удаления статьи по идентификатору.
     *
     * @param id Идентификатор статьи.
     * @return ResponseEntity со статусом удаления или статусом ошибки.
     */
    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        // Проверьте роль пользователя
        if (authentication != null) {
            boolean isAdmin = authentication.getAuthorities().stream()
                    .anyMatch(role -> role.getAuthority().equals("ROLE_ADMIN"));
            if (isAdmin) {
                Article objectForDelete = getService().findById(id);
                getService().delete(objectForDelete);
                return ResponseEntity.noContent().build();
            } else {
                return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
            }
        }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
    }

    /**
     * Метод получения статьи по её идентификатору.
     *
     * @param id Идентификатор статьи.
     * @return ResponseEntity с найденной статьей или статусом ошибки.
     */
    @Override
    @GetMapping("/find/{id}")
    public ResponseEntity<Article> findById(@PathVariable Long id) {
        Article object = getService().findById(id);
        if (object == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(object);
    }

    /**
     * Метод получения списка всех статей.
     *
     * @return ResponseEntity со списком статей или статусом ошибки.
     */
    @Override
    @GetMapping("/all")
    public ResponseEntity<List<Article>> findAll() {
        List<Article> objects = getService().findAll();
        return ResponseEntity.ok(objects);
    }

    /**
     * Метод получения списка последних статей.
     *
     * @return ResponseEntity со списком последних статей или статусом ошибки.
     */
    @GetMapping("/latest-articles")
    public ResponseEntity<List<Article>> getLatestArticles() {
        List<Article> latestArticles = articleService.getLatestArticles();
        return ResponseEntity.ok(latestArticles);
    }
}
