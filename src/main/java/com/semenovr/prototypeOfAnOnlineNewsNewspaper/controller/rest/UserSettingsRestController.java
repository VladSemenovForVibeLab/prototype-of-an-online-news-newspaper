package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.UserSettings;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CRUDService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserSettingsService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Этот класс представляет собой REST-контроллер для управления настройками пользователей и связанными с ними операциями.
 */
@RestController
@RequiredArgsConstructor
@RequestMapping(UserSettingsRestController.USER_SETTINGS_REST_URL)
public class UserSettingsRestController extends CRUDRestController<UserSettings, Long> {
    // Корневой URL для REST-контроллера настроек пользователя.
    public static final String USER_SETTINGS_REST_URL = "/api/v1/user-settings";

    private final UserSettingsService userSettingsService;

    @Override
    CRUDService<UserSettings, Long> getService() {
        return userSettingsService;
    }
}
