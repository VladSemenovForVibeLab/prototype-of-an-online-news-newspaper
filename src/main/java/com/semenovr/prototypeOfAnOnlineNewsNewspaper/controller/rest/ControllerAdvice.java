package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.*;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.naming.AuthenticationException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Глобальный обработчик исключений для контроллеров REST API.
 */
@RestControllerAdvice
public class ControllerAdvice {

    /**
     * Обработчик исключения LikeAlreadyExist.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(LikeAlreadyExist.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleLikeAlreadyExist(LikeAlreadyExist e) {
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения ResourceNotFoundException.
     * Возвращает статус NOT_FOUND и тело исключения.
     */
    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionBody handleResourceNotFound(ResourceNotFoundException e) {
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения ThemeNameAlreadyExistsException.
     * Возвращает статус NOT_FOUND и тело исключения.
     */
    @ExceptionHandler(ThemeNameAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ExceptionBody handleThemeNameAlreadyExists(ThemeNameAlreadyExistsException e) {
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения IllegalStateException.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(IllegalStateException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleIllegalState(IllegalStateException e) {
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения AccessDeniedException и AccessDeniedException из Spring Security.
     * Возвращает статус FORBIDDEN и тело с сообщением об отказе в доступе.
     */
    @ExceptionHandler({AccessDeniedException.class, org.springframework.security.access.AccessDeniedException.class})
    @ResponseStatus(HttpStatus.FORBIDDEN)
    public ExceptionBody handleAccessDenied() {
        return new ExceptionBody("Access. denied. ");
    }

    /**
     * Обработчик исключения MethodArgumentNotValidException.
     * Возвращает статус BAD_REQUEST и тело с сообщением о невалидных полях запроса.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        ExceptionBody exceptionBody = new ExceptionBody("Validation failed");
        List<FieldError> errors = e.getBindingResult().getFieldErrors();
        exceptionBody.setErrors(errors.stream()
                .collect(Collectors.toMap(FieldError::getField, FieldError::getDefaultMessage)));
        return exceptionBody;
    }

    /**
     * Обработчик исключения AuthenticationException.
     * Возвращает статус BAD_REQUEST и тело с сообщением о неудачной аутентификации.
     */
    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleAuthentication(AuthenticationException e){
        return new ExceptionBody("Authentication failed");
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ExceptionBody handleException(Exception e) {
        return new ExceptionBody("Internal error");
    }

    /**
     * Обработчик исключения ImageUploadException.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(ImageUploadException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleImageUpload(ImageUploadException e){
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения ArticleNotFoundException.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(ArticleNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleArticleNotFound(ArticleNotFoundException e){
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения UserNotFoundException.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(UserNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleUserNotFound(UserNotFoundException e){
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения LikeNotFoundException.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(LikeNotFoundException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleLikeNotFound(LikeNotFoundException e){
        return new ExceptionBody(e.getMessage());
    }

    /**
     * Обработчик исключения ExpiredJwtException.
     * Возвращает статус BAD_REQUEST и тело исключения.
     */
    @ExceptionHandler(ExpiredJwtException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ExceptionBody handleLikeNotFound(ExpiredJwtException e){
        return new ExceptionBody(e.getMessage());
    }
}

