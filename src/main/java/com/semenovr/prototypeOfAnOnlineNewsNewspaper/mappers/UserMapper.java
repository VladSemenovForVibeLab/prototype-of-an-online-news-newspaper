package com.semenovr.prototypeOfAnOnlineNewsNewspaper.mappers;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.user.UserDto;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.mapstruct.Mapper;

/**
 * Интерфейс UserMapper является компонентом MapStruct для маппинга между сущностями User и объектами UserDto.
 * Этот интерфейс предоставляет методы для преобразования объектов User в объекты UserDto и обратно.
 */
@Mapper(componentModel = "spring")
public interface UserMapper extends Mappable<User, UserDto> {

    /**
     * Преобразует объект UserDto в сущность User.
     *
     * @param dto Объект UserDto для преобразования.
     * @return Сущность User, созданная на основе объекта UserDto.
     */
    default User toEntity(UserDto dto) {
        if (dto == null) {
            return null;
        }
        // Создать новый объект User и установить значения из UserDto
        User entity = new User();
        entity.setUsername(dto.getUsername());
        entity.setFirstname(dto.getFirstname());
        entity.setLastName(dto.getLastName());
        entity.setPassword(dto.getPassword());
        entity.setPasswordConfirmation(dto.getPasswordConfirmation());
        // Установить другие поля, если необходимо
        return entity;
    }
}


