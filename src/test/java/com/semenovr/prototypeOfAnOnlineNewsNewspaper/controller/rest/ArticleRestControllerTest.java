package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.ArticleService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@WebMvcTest(ArticleRestController.class)
@ActiveProfiles("test")
public class ArticleRestControllerTest {

    @MockBean
    private UserService userService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private ArticleService articleService;

    @BeforeEach
    public void setUp() {

    }


    @Test
    @WithMockUser(roles = "USER") // Test unauthorized user
    public void testCreateArticleWithUserRole() throws Exception {
        Article articleToCreate = new Article(); // Set up article data

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/articles")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(articleToCreate)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void testCreateArticleWithoutAuthentication() throws Exception {
        Article articleToCreate = new Article(); // Set up article data

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/api/v1/articles")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(articleToCreate)))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }
}
