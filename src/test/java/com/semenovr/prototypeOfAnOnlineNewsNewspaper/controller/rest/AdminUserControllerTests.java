package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.AdminUserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@WebMvcTest(AdminUserController.class)
public class AdminUserControllerTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private AdminUserService adminUserService;
    @Test
    public void testAssignAdminRoleBadRequest() throws Exception {
        mockMvc.perform(post("/api/v1/admins/{userId}/assign-admin-role", 1L))
                .andExpect(status().isForbidden());
        verifyNoInteractions(adminUserService);
    }
}