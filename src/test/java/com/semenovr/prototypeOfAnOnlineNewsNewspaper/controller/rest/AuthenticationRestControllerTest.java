package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AuthenticationRestController.class)
@ActiveProfiles("test")
public class AuthenticationRestControllerTest {

}
