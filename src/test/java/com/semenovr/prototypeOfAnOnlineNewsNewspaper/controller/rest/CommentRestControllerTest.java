package com.semenovr.prototypeOfAnOnlineNewsNewspaper.controller.rest;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.CommentService;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

class CommentRestControllerTest {

    @Mock
    private CommentService commentService;

    @Mock
    private UserService userService;

    @InjectMocks
    private CommentRestController commentRestController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testGetCommentsByArticle() {
        Article article = new Article(); // create a sample article
        List<Comment> comments = Arrays.asList(new Comment(), new Comment()); // create sample comments
        when(commentService.getCommentsByArticle(article)).thenReturn(comments);

        ResponseEntity<?> response = commentRestController.getCommentsByArticle(article);

        verify(commentService, times(1)).getCommentsByArticle(article);
        assert response.getStatusCode() == HttpStatus.OK;
        assert response.getBody() == comments;
    }

    // Similarly, you can write tests for other methods in CommentRestController
}
