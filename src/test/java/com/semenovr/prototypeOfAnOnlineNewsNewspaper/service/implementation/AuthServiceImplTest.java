package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTRequest;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.dto.auth.JWTResponse;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.security.JwtTokenProvider;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.interf.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
class AuthServiceImplTest {

    @Mock
    private AuthenticationManager authenticationManager;

    @Mock
    private UserService userService;

    @Mock
    private JwtTokenProvider jwtTokenProvider;

    @InjectMocks
    private AuthServiceImpl authService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void login_ValidCredentials_ReturnJWTResponse() {
        JWTRequest loginRequest = new JWTRequest();
        loginRequest.setUsername("testuser");
        loginRequest.setPassword("testpassword");

        User user = new User();
        user.setId(1L);
        user.setUsername("testuser");
        when(userService.getByUsername("testuser")).thenReturn(user);

        when(jwtTokenProvider.createAccessToken(1L, "testuser", user.getRoles())).thenReturn("accessToken");
        when(jwtTokenProvider.createRefreshToken(1L, "testuser")).thenReturn("refreshToken");

        JWTResponse jwtResponse = authService.login(loginRequest);

        assertEquals(1L, jwtResponse.getId());
        assertEquals("testuser", jwtResponse.getUsername());
        assertEquals("accessToken", jwtResponse.getAccessToken());
        assertEquals("refreshToken", jwtResponse.getRefreshToken());

        verify(authenticationManager, times(1)).authenticate(new UsernamePasswordAuthenticationToken("testuser", "testpassword"));
        verify(userService, times(1)).getByUsername("testuser");
        verify(jwtTokenProvider, times(1)).createAccessToken(1L, "testuser", user.getRoles());
        verify(jwtTokenProvider, times(1)).createRefreshToken(1L, "testuser");
    }
}
