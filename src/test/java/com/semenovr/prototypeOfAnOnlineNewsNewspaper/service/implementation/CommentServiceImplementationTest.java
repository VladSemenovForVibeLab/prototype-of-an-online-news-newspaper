package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ArticleNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.CommentRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
class CommentServiceImplementationTest {

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private ArticleRepository articleRepository;

    @InjectMocks
    private CommentServiceImplementation commentService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getCommentsByArticle_ExistingArticle_ReturnListOfComments() {
        Article article = new Article();
        when(commentRepository.findByArticleOrderByWrittenDateDesc(article)).thenReturn(new ArrayList<>());

        List<Comment> comments = commentService.getCommentsByArticle(article);

        assertNotNull(comments);
        verify(commentRepository, times(1)).findByArticleOrderByWrittenDateDesc(article);
    }

    @Test
    void addCommentToArticle_ExistingArticle_ReturnAddedComment() {
        Long articleId = 1L;
        Article article = new Article();
        Comment commentDto = new Comment();
        commentDto.setAuthor(new User());
        commentDto.setText("Test comment");

        when(articleRepository.findById(articleId)).thenReturn(Optional.of(article));
        when(commentRepository.save(any())).thenReturn(commentDto);

        Comment addedComment = commentService.addCommentToArticle(articleId, commentDto);

        assertNotNull(addedComment);
        verify(articleRepository, times(1)).findById(articleId);
        verify(commentRepository, times(1)).save(any());
    }

    @Test
    void addCommentToArticle_NonExistentArticle_ThrowArticleNotFoundException() {
        Long articleId = 1L;
        Comment commentDto = new Comment();
        commentDto.setAuthor(new User());
        commentDto.setText("Test comment");

        when(articleRepository.findById(articleId)).thenReturn(Optional.empty());

        assertThrows(ArticleNotFoundException.class, () -> commentService.addCommentToArticle(articleId, commentDto));
        verify(articleRepository, times(1)).findById(articleId);
        verify(commentRepository, never()).save(any());
    }

    @Test
    void deleteCommentFromArticle_ExistingArticleAndComment_DeleteComment() {
        Long articleId = 1L;
        Long commentId = 2L;
        Article article = new Article();
        Comment comment = new Comment();
        comment.setArticle(article);

        when(articleRepository.findById(articleId)).thenReturn(Optional.of(article));
        when(commentRepository.findById(commentId)).thenReturn(Optional.of(comment));

        assertDoesNotThrow(() -> commentService.deleteCommentFromArticle(articleId, commentId));

        verify(articleRepository, times(1)).findById(articleId);
        verify(commentRepository, times(1)).findById(commentId);
        verify(commentRepository, times(1)).delete(comment);
    }

    @Test
    void deleteCommentFromArticle_NonExistentArticle_ThrowArticleNotFoundException() {
        Long articleId = 1L;
        Long commentId = 2L;

        when(articleRepository.findById(articleId)).thenReturn(Optional.empty());

        assertThrows(ArticleNotFoundException.class, () -> commentService.deleteCommentFromArticle(articleId, commentId));
        verify(articleRepository, times(1)).findById(articleId);
        verify(commentRepository, never()).findById(commentId);
        verify(commentRepository, never()).delete(any());
    }
}
