package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.LikeAlreadyExist;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.LikeNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.LikeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
class LikeServiceImplementationTest {

    @Mock
    private LikeRepository likeRepository;

    @InjectMocks
    private LikeServiceImplementation likeService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void likeArticle_UserAlreadyLiked_ThrowLikeAlreadyExistException() {
        Article article = new Article();
        User user = new User();

        when(likeRepository.existsByArticleAndUser(article, user)).thenReturn(true);

        assertThrows(LikeAlreadyExist.class, () -> likeService.likeArticle(article, user));
        verify(likeRepository, never()).save(any());
    }

    @Test
    void unlikeArticle_UserLiked_RemoveLike() {
        Article article = new Article();
        User user = new User();
        Like like = new Like();
        like.setArticle(article);
        like.setUser(user);

        when(likeRepository.findByArticleAndUser(article, user)).thenReturn(like);

        assertDoesNotThrow(() -> likeService.unlikeArticle(article, user));

        verify(likeRepository, times(1)).delete(like);
    }

    @Test
    void unlikeArticle_UserNotLiked_ThrowLikeNotFoundException() {
        Article article = new Article();
        User user = new User();

        when(likeRepository.findByArticleAndUser(article, user)).thenReturn(null);

        assertThrows(LikeNotFoundException.class, () -> likeService.unlikeArticle(article, user));
        verify(likeRepository, never()).delete(any());
    }

    @Test
    void hasUserLikedArticle_UserLiked_ReturnTrue() {
        Article article = new Article();
        User user = new User();

        when(likeRepository.existsByArticleAndUser(article, user)).thenReturn(true);

        boolean hasLiked = likeService.hasUserLikedArticle(article, user);

        assertTrue(hasLiked);
    }

    @Test
    void hasUserLikedArticle_UserNotLiked_ReturnFalse() {
        Article article = new Article();
        User user = new User();

        when(likeRepository.existsByArticleAndUser(article, user)).thenReturn(false);

        boolean hasLiked = likeService.hasUserLikedArticle(article, user);

        assertFalse(hasLiked);
    }
}
