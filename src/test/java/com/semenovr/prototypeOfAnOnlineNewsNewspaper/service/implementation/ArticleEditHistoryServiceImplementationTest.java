package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.ArticleEditHistory;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.DuplicateCombinationException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ResourceNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleEditHistoryRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class ArticleEditHistoryServiceImplementationTest {

    @Mock
    private ArticleEditHistoryRepository articleEditHistoryRepository;

    @Mock
    private ArticleRepository articleRepository;

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private ArticleEditHistoryServiceImplementation articleEditHistoryService;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetEditHistoryForArticle_ArticleNotFound() {
        Long articleId = 1L;

        when(articleRepository.findById(articleId)).thenReturn(Optional.empty());

        assertThrows(ResourceNotFoundException.class, () -> {
            articleEditHistoryService.getEditHistoryForArticle(articleId);
        });
    }

}