package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.ArticleTitleExistsException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.ArticleRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.CommentRepository;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.LikeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
class ArticleServiceImplementationTest {

    @Mock
    private ArticleRepository articleRepository;

    @Mock
    private CommentRepository commentRepository;

    @Mock
    private LikeRepository likeRepository;

    @InjectMocks
    private ArticleServiceImplementation articleService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void create_NewArticle_SuccessfulCreation() {
        Article article = new Article();
        when(articleRepository.existsByTitle(any())).thenReturn(false);

        assertDoesNotThrow(() -> articleService.create(article));

        verify(articleRepository, times(1)).existsByTitle(any());
        verify(articleRepository, times(1)).save(article);
    }

    @Test
    void create_ArticleWithTitleExists_ExceptionThrown() {
        Article article = new Article();
        when(articleRepository.existsByTitle(any())).thenReturn(true);

        assertThrows(ArticleTitleExistsException.class, () -> articleService.create(article));
        verify(articleRepository, times(1)).existsByTitle(any());
        verify(articleRepository, never()).save(any());
    }

    @Test
    void delete_DeleteArticleAndAssociatedCommentsAndLikes_SuccessfulDeletion() {
        Article article = new Article();
        List<Comment> comments = new ArrayList<>();
        List<Like> likes = new ArrayList<>();
        article.setComments(comments);
        when(likeRepository.findByArticle(article)).thenReturn(likes);

        assertDoesNotThrow(() -> articleService.delete(article));

        verify(commentRepository, times(1)).deleteAll(comments);
        verify(likeRepository, times(1)).deleteAll(likes);
        verify(articleRepository, times(1)).delete(article);
    }
}
