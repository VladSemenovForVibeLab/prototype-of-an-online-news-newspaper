package com.semenovr.prototypeOfAnOnlineNewsNewspaper.service.implementation;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.exception.UserNotFoundException;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@SpringBootTest
@ActiveProfiles("test")
public class AdminUserServiceImplementationTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private AdminUserServiceImplementation adminUserService;

    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAssignAdminRole() {
        Long userId = 1L;
        User user = new User();
        user.setId(userId);
        user.setRoles(new HashSet<>()); // Предполагаем, что у пользователя нет ролей

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        adminUserService.assignAdminRole(userId);

        Set<Role> updatedRoles = user.getRoles();
        assertTrue(updatedRoles.contains(Role.ROLE_ADMIN));

        // Проверяем, что метод save был вызван в userRepository
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void testAssignAdminRole_UserNotFound() {
        Long userId = 1L;

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> {
            adminUserService.assignAdminRole(userId);
        });

        // Проверяем, что метод save НЕ вызывался в userRepository
        verify(userRepository, never()).save(any());
    }
}
