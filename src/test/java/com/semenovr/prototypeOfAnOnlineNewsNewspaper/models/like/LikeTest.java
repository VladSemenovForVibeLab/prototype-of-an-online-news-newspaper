package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
public class LikeTest {

    private Like like;
    private Article article;
    private User user;

    @BeforeEach
    public void setUp() {
        like = new Like();
        article = new Article();
        user = new User();

        article.setTitle("Sample Article");
        user.setUsername("user");

        like.setArticle(article);
        like.setUser(user);
    }

    @Test
    public void testLikeInitialization() {
        assertNotNull(like);
        assertNotNull(like.getArticle());
        assertNotNull(like.getUser());
        assertEquals("Sample Article", like.getArticle().getTitle());
        assertEquals("user", like.getUser().getUsername());
    }
}
