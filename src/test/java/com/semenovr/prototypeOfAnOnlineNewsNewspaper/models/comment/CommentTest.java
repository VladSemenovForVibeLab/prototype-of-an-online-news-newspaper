package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
public class CommentTest {

    private Comment comment;
    private Article article;
    private User author;

    @BeforeEach
    public void setUp() {
        comment = new Comment();
        article = new Article();
        author = new User();

        article.setTitle("Sample Article");
        author.setUsername("user");

        comment.setArticle(article);
        comment.setAuthor(author);
        comment.setText("This is a sample comment");
        comment.setWrittenDate(LocalDateTime.now());
    }

    @Test
    public void testCommentInitialization() {
        assertNotNull(comment);
        assertNotNull(comment.getArticle());
        assertNotNull(comment.getAuthor());
        assertNotNull(comment.getWrittenDate());
        assertEquals("Sample Article", comment.getArticle().getTitle());
        assertEquals("user", comment.getAuthor().getUsername());
        assertEquals("This is a sample comment", comment.getText());
    }
}
