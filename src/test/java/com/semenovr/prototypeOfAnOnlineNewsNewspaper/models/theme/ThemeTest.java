package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
public class ThemeTest {

    private Theme theme;

    @BeforeEach
    public void setUp() {
        theme = new Theme();
        theme.setId(1L);
        theme.setName("Sample Theme");

        Set<Article> articles = new HashSet<>();
        Article article1 = new Article();
        article1.setTitle("Article 1");

        Article article2 = new Article();
        article2.setTitle("Article 2");

        articles.add(article1);
        articles.add(article2);

        theme.setArticles(articles);
    }

    @Test
    public void testThemeInitialization() {
        assertNotNull(theme);
        assertEquals(1L, theme.getId());
        assertEquals("Sample Theme", theme.getName());
        assertNotNull(theme.getArticles());
        assertEquals(2, theme.getArticles().size());
    }
}