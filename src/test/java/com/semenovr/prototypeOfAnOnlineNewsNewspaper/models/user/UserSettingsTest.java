package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
@ActiveProfiles("test")
public class UserSettingsTest {

    private UserSettings userSettings;

    @BeforeEach
    public void setUp() {
        userSettings = new UserSettings();
        userSettings.setId(1L);

        User user = new User();
        user.setUsername("sample_user");
        userSettings.setUser(user);

        Set<Theme> favoriteThemes = new HashSet<>();
        Theme theme1 = new Theme();
        theme1.setName("Theme 1");

        Theme theme2 = new Theme();
        theme2.setName("Theme 2");

        favoriteThemes.add(theme1);
        favoriteThemes.add(theme2);

        userSettings.setFavoriteThemes(favoriteThemes);

        Set<Theme> forbiddenThemes = new HashSet<>();
        Theme theme3 = new Theme();
        theme3.setName("Theme 3");

        forbiddenThemes.add(theme3);

        userSettings.setForbiddenThemes(forbiddenThemes);
    }

    @Test
    public void testUserSettingsInitialization() {
        assertNotNull(userSettings);
        assertEquals(1L, userSettings.getId());
        assertNotNull(userSettings.getUser());
        assertEquals("sample_user", userSettings.getUser().getUsername());
        assertNotNull(userSettings.getFavoriteThemes());
        assertEquals(2, userSettings.getFavoriteThemes().size());
        assertNotNull(userSettings.getForbiddenThemes());
        assertEquals(1, userSettings.getForbiddenThemes().size());
    }
}
