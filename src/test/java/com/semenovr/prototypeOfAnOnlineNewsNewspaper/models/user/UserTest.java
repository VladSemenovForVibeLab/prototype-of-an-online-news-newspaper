package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.like.Like;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
@ActiveProfiles("test")
public class UserTest {

    private User user;

    @BeforeEach
    public void setUp() {
        user = new User();
        user.setId(1L);
        user.setUsername("sample_user");
        user.setFirstname("John");
        user.setLastName("Doe");
        user.setPassword("password");
        user.setPasswordConfirmation("password");
        Set<com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role> roles = new HashSet<>();
        roles.add(Role.ROLE_USER);
        user.setRoles(roles);

        List<Article> articles = new ArrayList<>();
        user.setArticles(articles);

        List<Comment> comments =new  ArrayList<>();
        user.setComments(comments);

        Set<Like> likes =new HashSet<>();
        user.setLikes(likes);

        UserSettings userSettings = new UserSettings();
        userSettings.setUser(user);
        user.setUserSettings(userSettings);
    }

    @Test
    public void testUserInitialization() {
        assertNotNull(user);
        assertEquals(1L, user.getId());
        assertEquals("sample_user", user.getUsername());
        assertEquals("John", user.getFirstname());
        assertEquals("Doe", user.getLastName());
        assertEquals("password", user.getPassword());
        assertEquals("password", user.getPasswordConfirmation());
        assertNotNull(user.getRoles());
        assertEquals(1, user.getRoles().size());
        assertNotNull(user.getArticles());
    }
}