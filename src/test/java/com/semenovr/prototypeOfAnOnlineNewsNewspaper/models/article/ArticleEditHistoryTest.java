package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
public class ArticleEditHistoryTest {
    private ArticleEditHistory history;
    private Article article;
    private User administrator;

    @BeforeEach
    public void setUp() {
        history = new ArticleEditHistory();
        article = new Article();
        administrator = new User();

        article.setTitle("Sample Article");
        administrator.setUsername("admin");

        history.setArticle(article);
        history.setAdministrator(administrator);
        history.setEditDate(LocalDateTime.now());
    }

    @Test
    public void testArticleEditHistoryInitialization() {
        assertNotNull(history);
        assertNotNull(history.getArticle());
        assertNotNull(history.getAdministrator());
        assertNotNull(history.getEditDate());
        assertEquals("Sample Article", history.getArticle().getTitle());
        assertEquals("admin", history.getAdministrator().getUsername());
    }
}
