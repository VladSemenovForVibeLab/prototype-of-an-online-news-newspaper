package com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.article.Article;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.comment.Comment;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.theme.Theme;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest
public class ArticleTest {

    private Article article;

    @BeforeEach
    public void setUp() {
        article = new Article();
        article.setTitle("Sample Article");
        article.setText("This is a sample article text.");
        article.setImageUrl("sample.jpg");
        article.setPublicationDate(LocalDateTime.now());

        User author = new User();
        author.setUsername("author");
        article.setAuthor(author);

        Comment comment1 = new Comment();
        comment1.setText("Comment 1");
        Comment comment2 = new Comment();
        comment2.setText("Comment 2");
        List<Comment> comments = new ArrayList<>();
        comments.add(comment1);
        comments.add(comment2);
        article.setComments(comments);

        Theme theme1 = new Theme();
        theme1.setName("Theme 1");
        Theme theme2 = new Theme();
        theme2.setName("Theme 2");
        Set<Theme> themes = new HashSet<>();
        themes.add(theme1);
        themes.add(theme2);
        article.setThemes(themes);
    }

    @Test
    public void testArticleInitialization() {
        assertNotNull(article);
        assertEquals("Sample Article", article.getTitle());
        assertEquals("This is a sample article text.", article.getText());
        assertEquals("sample.jpg", article.getImageUrl());
        assertNotNull(article.getPublicationDate());
        assertNotNull(article.getAuthor());
    }

    @Test
    public void testArticleComments() {
        List<Comment> comments = article.getComments();
        assertNotNull(comments);
        assertEquals(2, comments.size());
    }

    @Test
    public void testArticleThemes() {
        Set<Theme> themes = article.getThemes();
        assertNotNull(themes);
        assertEquals(2, themes.size());
    }
}
