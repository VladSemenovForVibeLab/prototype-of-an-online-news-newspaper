package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import org.junit.jupiter.api.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
public class JwtTokenFilterTest {

    @Test
    public void testDoFilter() throws IOException, ServletException {
        // Создайте мок-объект JwtTokenProvider
        JwtTokenProvider jwtTokenProvider = mock(JwtTokenProvider.class);

        // Создайте фильтр JwtTokenFilter с мок-объектом JwtTokenProvider
        JwtTokenFilter jwtTokenFilter = new JwtTokenFilter(jwtTokenProvider);

        // Создайте мок-запрос с заголовком "Authorization"
        MockHttpServletRequest request = new MockHttpServletRequest();
        request.addHeader("Authorization", "Bearer your_jwt_token_here");

        // Создайте мок-ответ
        MockHttpServletResponse response = new MockHttpServletResponse();

        // Создайте мок-цепочку фильтров
        FilterChain filterChain = mock(FilterChain.class);

        // Создайте мок-объект Authentication
        Authentication authentication = mock(Authentication.class);

        // Установите мок-объект Authentication в SecurityContextHolder
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // Установите поведение для JwtTokenProvider
        when(jwtTokenProvider.validateToken(anyString())).thenReturn(true);
        when(jwtTokenProvider.getAuthentication(anyString())).thenReturn(authentication);

        // Вызовите метод doFilter
        jwtTokenFilter.doFilter(request, response, filterChain);

        // Проверьте, что методы JwtTokenProvider были вызваны
        verify(jwtTokenProvider).validateToken("your_jwt_token_here");
        verify(jwtTokenProvider).getAuthentication("your_jwt_token_here");

        // Проверьте, что фильтр передал управление следующему фильтру в цепочке
        verify(filterChain).doFilter(request, response);

        // Проверьте, что объект Authentication был установлен в SecurityContextHolder
        assertEquals(authentication, SecurityContextHolder.getContext().getAuthentication());
    }
}
