package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.User;
import com.semenovr.prototypeOfAnOnlineNewsNewspaper.models.user.enums.Role;
import org.h2.command.dml.Set;
import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
public class JwtEntityFactoryTest {

    @Test
    public void testCreate() {
        // Создайте объект пользователя
        User user = new User();
        user.setId(1L);
        user.setUsername("username");
        user.setFirstname("John");
        user.setLastName("Doe");
        user.setPassword("password");
        user.setRoles(new HashSet<>());

        // Вызовите метод create() для создания объекта JwtEntity
        JwtEntity jwtEntity = JwtEntityFactory.create(user);

        // Проверьте, что поля JwtEntity соответствуют ожидаемым значениям
        assertEquals(user.getId(), jwtEntity.getId());
        assertEquals(user.getUsername(), jwtEntity.getUsername());
        assertEquals(user.getFirstname(), jwtEntity.getFirstname());
        assertEquals(user.getLastName(), jwtEntity.getLastName());
        assertEquals(user.getPassword(), jwtEntity.getPassword());
        assertEquals(0, jwtEntity.getAuthorities().size()); // Пустой список ролей
    }
}