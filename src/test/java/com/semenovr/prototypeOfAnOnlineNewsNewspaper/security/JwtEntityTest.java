package com.semenovr.prototypeOfAnOnlineNewsNewspaper.security;

import org.junit.jupiter.api.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ActiveProfiles("test")
public class JwtEntityTest {

    @Test
    public void testGetAuthorities() {
        // Создайте объект JwtEntity с набором ролей
        Collection<? extends GrantedAuthority> authorities = Collections.singleton(new SimpleGrantedAuthority("ROLE_USER"));
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", authorities);

        // Проверьте, что метод getAuthorities возвращает ожидаемую коллекцию ролей
        assertEquals(authorities, jwtEntity.getAuthorities());
    }

    @Test
    public void testGetPassword() {
        // Создайте объект JwtEntity с паролем
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", Collections.emptyList());

        // Проверьте, что метод getPassword возвращает ожидаемый пароль
        assertEquals("password", jwtEntity.getPassword());
    }

    @Test
    public void testGetUsername() {
        // Создайте объект JwtEntity с именем пользователя
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", Collections.emptyList());

        // Проверьте, что метод getUsername возвращает ожидаемое имя пользователя
        assertEquals("username", jwtEntity.getUsername());
    }

    @Test
    public void testIsAccountNonExpired() {
        // Создайте объект JwtEntity
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", Collections.emptyList());

        // Проверьте, что метод isAccountNonExpired всегда возвращает true
        assertTrue(jwtEntity.isAccountNonExpired());
    }

    @Test
    public void testIsAccountNonLocked() {
        // Создайте объект JwtEntity
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", Collections.emptyList());

        // Проверьте, что метод isAccountNonLocked всегда возвращает true
        assertTrue(jwtEntity.isAccountNonLocked());
    }

    @Test
    public void testIsCredentialsNonExpired() {
        // Создайте объект JwtEntity
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", Collections.emptyList());

        // Проверьте, что метод isCredentialsNonExpired всегда возвращает true
        assertTrue(jwtEntity.isCredentialsNonExpired());
    }

    @Test
    public void testIsEnabled() {
        // Создайте объект JwtEntity
        JwtEntity jwtEntity = new JwtEntity(1L, "username", "John", "Doe", "password", Collections.emptyList());

        // Проверьте, что метод isEnabled всегда возвращает true
        assertTrue(jwtEntity.isEnabled());
    }
}
