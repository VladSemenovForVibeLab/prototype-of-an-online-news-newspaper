FROM maven:3.6.3-jdk-8 AS build
WORKDIR /
COPY /src /src
COPY pom.xml /
RUN mvn -f /pom.xml clean package -DskipTests

FROM openjdk:8-jre-slim
WORKDIR /
COPY /src /src
COPY --from=build /target/*.jar onlineNews.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "onlineNews.jar"]